package anime.heroes.projekt.game.cost;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import static com.shazam.shazamcrest.MatcherAssert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThan;

class RandomIntegerGeneratorServiceTest {

    @InjectMocks
    private RandomIntegerGeneratorService randomIntegerGeneratorService;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @Test
    void givenRangeWhenGetRandomNumberInRangeThenReturnNumberBetweenTheRange(){
        //given
        List<Integer> randomOutputs = new ArrayList<>();

        //when
        IntStream.range(0, 100).forEach(index -> randomOutputs.add(randomIntegerGeneratorService.getRandomNumberInRange(0, 3)));

        //then
        randomOutputs.forEach(result -> {
            assertThat(result, greaterThan(-1));
            assertThat(result, lessThan(4));
        });
    }

}
