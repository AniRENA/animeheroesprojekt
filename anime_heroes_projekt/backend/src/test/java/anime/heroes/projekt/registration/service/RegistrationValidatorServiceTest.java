package anime.heroes.projekt.registration.service;

import static org.mockito.MockitoAnnotations.initMocks;
import static com.shazam.shazamcrest.MatcherAssert.assertThat;
import static com.shazam.shazamcrest.matcher.Matchers.sameBeanAs;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import anime.heroes.projekt.entity.GameUser;
import anime.heroes.projekt.repository.GameUserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;

class RegistrationValidatorServiceTest {

    @InjectMocks
    private RegistrationValidatorService registrationValidatorService = new RegistrationValidatorService();

    @Mock
    private GameUserRepository gameUserRepository;

    @Mock
    private GameUser gameUser;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @Test
    void givenExistingUserNameWhenIsUserNameValidThenReturnNotValid() {
        //given
        when(gameUserRepository.findByUserName(any())).thenReturn(gameUser);
        when(gameUser.getUserName()).thenReturn("example");

        //when
        boolean result = registrationValidatorService.isUserNameValid("example");

        //then
        assertThat(result, sameBeanAs(false));
    }

    @Test
    void givenNotExistingUserNameWhenIsUserNameValidThenReturnValid() {
        //given
        when(gameUserRepository.findByUserName(any())).thenReturn(null);

        //when
        boolean result = registrationValidatorService.isUserNameValid("example");

        //then
        assertThat(result, sameBeanAs(true));
    }

    @Test
    void givenInvalidPasswordWhenIsUserNameValidThenReturnFalse() {
        //given

        //when
        boolean result = registrationValidatorService.isPasswordValid("exam");

        //then
        assertThat(result, sameBeanAs(false));
    }

    @Test
    void givenInvalidPasswordWhenIsUserNameValidThenReturnTrue() {
        //given

        //when
        boolean result = registrationValidatorService.isPasswordValid("exampleLongValue");

        //then
        assertThat(result, sameBeanAs(true));
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "plainaddress",
            "#@%^%#$@#$@#.com",
            "@example.com",
            "Joe Smith <email@example.com>",
            "email.example.com",
            "email@example@example.com",
            ".email@example.com",
            "email.@example.com",
            "email..email@example.com",
            "email@example.com (Joe Smith)",
            "email@example",
            "email@-example.com",
            "email@example.web",
            "email@111.222.333.44444",
            "email@example..com",
            "Abc..123@example.com",
            "(),:;<>[\\]@example.com",
            "this\\ is\"really\"not\\allowed@example.com",
    })
    void givenInvalidEmailWhenIsValidEmailThenReturnFalse(String email) {
        //given

        //when
        boolean result = registrationValidatorService.isValidEmail(email);

        //then
        assertThat(result, sameBeanAs(false));
    }

    @ParameterizedTest
    @ValueSource(strings = {
                    "email@example.com",
                    "firstname.lastname@example.com",
                    "email@subdomain.example.com",
                    "firstname+lastname@example.com",
                    "email@[123.123.123.123]",
                    "\"email\"@example.com",
                    "1234567890@example.com",
                    "email@example-one.com",
                    "_______@example.com",
                    "email@example.name",
                    "email@example.museum",
                    "email@example.co.jp",
                    "firstname-lastname@example.com"
    })
    void givenValidEmailWhenIsValidEmailThenReturnTrue(String email) {
        //given

        //when
        boolean result = registrationValidatorService.isValidEmail(email);

        //then
        assertThat(result, sameBeanAs(true));
    }
}
