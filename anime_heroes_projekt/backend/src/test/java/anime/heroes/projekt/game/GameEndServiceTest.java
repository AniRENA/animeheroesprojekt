package anime.heroes.projekt.game;

import anime.heroes.projekt.changeTurn.dto.TurnChangeCharacter;
import anime.heroes.projekt.entity.GameUser;
import anime.heroes.projekt.entity.json.gameState.State;
import anime.heroes.projekt.repository.GameStateRepository;
import anime.heroes.projekt.repository.GameUserRepository;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import static com.shazam.shazamcrest.MatcherAssert.assertThat;
import static com.shazam.shazamcrest.matcher.Matchers.sameBeanAs;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

class GameEndServiceTest {

    @Mock
    private GameUserRepository gameUserRepository;

    @Mock
    private GameStateRepository gameStateRepository;

    @Mock
    private GameUser gameUser1;

    @Mock
    private GameUser gameUser2;

    @InjectMocks
    private GameEndService gameEndService;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @ParameterizedTest(name = "{index} => firstFriendlyHealth={0}, secondFriendlyHealth={1}, thirdFriendlyHealth={2}, firstEnemyHealth={3}, secondEnemyHealth={4}, thirdEnemyHealth={5}")
    @CsvSource({
            "10, 10, 10, 10, 10, 10",
            "0, 10, 10, 0, 10, 10",
            "0, 0, 10, 0, 0, 10"
    })
    void givenEveryPlayerHasAtLeastOneAliveCharacterWhenIsGameOverThenReturnFalse(int firstFriendlyHealth, int secondFriendlyHealth, int thirdFriendlyHealth,
                                                                 int firstEnemyHealth, int secondEnemyHealth, int thirdEnemyHealth){
        //given
        State state = State.builder()
                .playerOneCharacters(Lists.newArrayList(
                        TurnChangeCharacter.builder().actualHealth(firstFriendlyHealth).build(),
                        TurnChangeCharacter.builder().actualHealth(secondFriendlyHealth).build(),
                        TurnChangeCharacter.builder().actualHealth(thirdFriendlyHealth).build()))
                .playerTwoCharacters(Lists.newArrayList(
                        TurnChangeCharacter.builder().actualHealth(firstEnemyHealth).build(),
                        TurnChangeCharacter.builder().actualHealth(secondEnemyHealth).build(),
                        TurnChangeCharacter.builder().actualHealth(thirdEnemyHealth).build()))
                .build();

        //when
        boolean result = gameEndService.isGameOver(state);

        //then
        assertThat(result, sameBeanAs(false));
    }

    @ParameterizedTest(name = "{index} => firstFriendlyHealth={0}, secondFriendlyHealth={1}, thirdFriendlyHealth={2}, firstEnemyHealth={3}, secondEnemyHealth={4}, thirdEnemyHealth={5}")
    @CsvSource({
            "0, 0, 0, 10, 10, 10",
            "0, 10, 10, 0, 0, 0",
            "0, 0, 0, 0, 0, 0"
    })
    void givenAtLeastOnePlayerIsOutOfCharactersWhenIsGameOverThenReturnFalse(int firstFriendlyHealth, int secondFriendlyHealth, int thirdFriendlyHealth,
                                                                                  int firstEnemyHealth, int secondEnemyHealth, int thirdEnemyHealth){
        //given
        State state = State.builder()
                .playerOneCharacters(Lists.newArrayList(
                        TurnChangeCharacter.builder().actualHealth(firstFriendlyHealth).build(),
                        TurnChangeCharacter.builder().actualHealth(secondFriendlyHealth).build(),
                        TurnChangeCharacter.builder().actualHealth(thirdFriendlyHealth).build()))
                .playerTwoCharacters(Lists.newArrayList(
                        TurnChangeCharacter.builder().actualHealth(firstEnemyHealth).build(),
                        TurnChangeCharacter.builder().actualHealth(secondEnemyHealth).build(),
                        TurnChangeCharacter.builder().actualHealth(thirdEnemyHealth).build()))
                .build();

        //when
        boolean result = gameEndService.isGameOver(state);

        //then
        assertThat(result, sameBeanAs(true));
    }

    @Test
    void givenEndGameWhenRefreshDatabaseWithGameResultThenPersist(){
        //given
        State state = State.builder()
                .playerOneName("fremen")
                .playerTwoName("senyor")
                .playerOneCharacters(Lists.newArrayList(
                        TurnChangeCharacter.builder().actualHealth(0).build(),
                        TurnChangeCharacter.builder().actualHealth(0).build(),
                        TurnChangeCharacter.builder().actualHealth(0).build()))
                .playerTwoCharacters(Lists.newArrayList(
                        TurnChangeCharacter.builder().actualHealth(10).build(),
                        TurnChangeCharacter.builder().actualHealth(10).build(),
                        TurnChangeCharacter.builder().actualHealth(10).build()))
                .build();

        when(gameUserRepository.findByUserName("fremen")).thenReturn(gameUser1);
        when(gameUserRepository.findByUserName("senyor")).thenReturn(gameUser2);

        //when
        gameEndService.refreshDatabaseWithGameResult(state);

        //then
        Mockito.verify(gameUserRepository, times(1)).save(gameUser1);
        Mockito.verify(gameUserRepository).save(gameUser2);
    }

}
