CREATE TABLE game_state (
  player_one VARCHAR(64) NOT NULL,
  player_two VARCHAR(64) NOT NULL,
  game_type VARCHAR(64) NOT NULL,
  state MEDIUMTEXT NOT NULL,
  PRIMARY KEY (player_one, player_two)
);
