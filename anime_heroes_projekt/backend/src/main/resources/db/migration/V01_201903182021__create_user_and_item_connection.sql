CREATE TABLE game_item (
    id   INTEGER PRIMARY KEY,
    name VARCHAR(64) NOT NULL UNIQUE
);

INSERT INTO game_item
    VALUES
    (1, 'AMONS_MASK'),
    (2, 'BLAZE_SWORD'),
    (3, 'BLOOD_SWORD'),
    (4, 'DEMON_SWORD'),
    (5, 'GOLDEN_EYE'),
    (6, 'LIGHT_SABER'),
    (7, 'MACHINE_GUN'),
    (8, 'MAKE_UP_SET'),
    (9, 'MOTOR_BIKE');

CREATE TABLE game_user_game_item(
    game_user_id INTEGER NOT NULL,
    game_item_id  INTEGER NOT NULL,
    UNIQUE (game_user_id, game_item_id)
);

INSERT INTO game_user_game_item
    VALUES
    (1,1),
    (1,3),
    (1,4),
    (1,6),
    (1,7),
    (1,8),
    (2,8),
    (2,2),
    (2,4);

