CREATE TABLE game_user_game_character (
  game_user_id INTEGER NOT NULL,
  game_character_id  INTEGER NOT NULL,
  UNIQUE (game_user_id, game_character_id)
);

INSERT INTO game_user_game_character
    VALUES
    (1,1),
    (1,2),
    (1,3),
    (1,4),
    (1,5),
    (1,6),
    (1,7),
    (1,8),
    (2,8),
    (2,9),
    (2,10),
    (2,11),
    (2,12),
    (2,13),
    (2,14),
    (2,15),
    (2,16);
