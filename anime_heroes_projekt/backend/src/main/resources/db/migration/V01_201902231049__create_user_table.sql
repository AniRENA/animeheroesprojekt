CREATE TABLE game_user (
  id          INTEGER PRIMARY KEY AUTO_INCREMENT,
  username VARCHAR(64) NOT NULL UNIQUE,
  password   VARCHAR(64) NOT NULL,
  rank VARCHAR(64) NOT NULL,
  clan VARCHAR(64),
  lvl INTEGER NOT NULL,
  ladder_rank INTEGER NOT NULL,
  wins INTEGER NOT NULL,
  loses INTEGER NOT NULL,
  ratio INTEGER NOT NULL
);

INSERT INTO game_user
    VALUES
        (-1, 'itachiviktor', 'Rohadjmeg1', "HOBBY_HEROE", "alma", 45, 1, 100, 0, 100),
        (-2, 'senyor', 'pink', "WARRIOR", "asd noobs", 2, 3000, 0, 20, -20);

CREATE TABLE game_character (
    id   INTEGER PRIMARY KEY,
    name VARCHAR(64) NOT NULL UNIQUE
);

INSERT INTO game_character
    VALUES
    (1, 'AANG'),
    (2, 'AKENO'),
    (3, 'ALLEN'),
    (4, 'AMI'),
    (5, 'KIRITO'),
    (6, 'KATARA'),
    (7, 'EDWARD'),
    (8, 'ELENA'),
    (9, 'LEVI'),
    (10, 'ZUKO'),
    (11, 'ASUNA'),
    (12, 'NARUTO'),
    (13, 'LAURA'),
    (14, 'GENOS'),
    (15, 'INUYASHA'),
    (16, 'JELLAL'),
    (17, 'KAZUKI'),
    (18, 'KIRA'),
    (19, 'KORRA'),
    (20, 'KURUMI'),
    (21, 'MAYURI'),
    (22, 'MIRIA'),
    (23, 'RIAS'),
    (24, 'RIZE'),
    (25, 'ROY'),
    (26, 'SAEKO'),
    (27, 'SAITAMA'),
    (28, 'ICHIGO'),
    (29, 'SHINJI'),
    (30, 'ARMSTRONG'),
    (31, 'SCAR'),
    (32, 'TERESA'),
    (33, 'TOSHIRO'),
    (34, 'ERZA'),
    (35, 'YUGI'),
    (36, 'YUNO'),
    (37, 'YUUKO'),
    (38, 'DOFLAMINGO'),
    (39, 'ZORO');
