package anime.heroes.projekt.entity;

import anime.heroes.projekt.common.ItemName;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "game_item")
public class GameItem {

    @Id
    private int id;

    @Enumerated(EnumType.STRING)
    private ItemName name;

}
