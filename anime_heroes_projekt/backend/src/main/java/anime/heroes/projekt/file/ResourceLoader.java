package anime.heroes.projekt.file;

import anime.heroes.projekt.common.CharacterName;
import anime.heroes.projekt.common.ItemName;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

@Service
public class ResourceLoader {

    public String readCharacterAsJSON(CharacterName name) throws IOException {
        File file = ResourceUtils.getFile("classpath:character_customization/" + name.getValue() + ".json");
        return new String(Files.readAllBytes(file.toPath()));
    }

    public String readItemAsJSON(ItemName name) throws IOException {
        File file = ResourceUtils.getFile("classpath:item_customization/" + name.getValue() + ".json");
        return new String(Files.readAllBytes(file.toPath()));
    }

    /**
     * Az aktív itemek külön leíróban vannak, mert azok olyanok mint a skillek, tehát minden aktív itemhez
     * kell legyen egy json az active_item_customization mappában.
     * */
    public String readActiveItemAsSkillAsJson(ItemName name)throws IOException {
        File file = ResourceUtils.getFile("classpath:active_item_customization/" + name.getValue() + ".json");
        return new String(Files.readAllBytes(file.toPath()));
    }
}
