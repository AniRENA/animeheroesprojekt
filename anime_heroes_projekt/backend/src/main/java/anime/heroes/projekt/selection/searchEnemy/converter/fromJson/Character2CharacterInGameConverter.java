package anime.heroes.projekt.selection.searchEnemy.converter.fromJson;

import anime.heroes.projekt.selection.searchEnemy.dto.*;
import anime.heroes.projekt.selection.searchEnemy.dto.fromJson.Character;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class Character2CharacterInGameConverter {

    private Skill2SkillInGameConverter skill2SkillInGameConverter;

    public Character2CharacterInGameConverter(Skill2SkillInGameConverter skill2SkillInGameConverter) {
        this.skill2SkillInGameConverter = skill2SkillInGameConverter;
    }

    public CharacterInGame convertWithoutItems(Character character) {
        CharacterInGame characterInGame = new CharacterInGame();
        characterInGame.setName(character.getName());
        characterInGame.setHealth(character.getHealth());
        characterInGame.setActualHealth(character.getHealth());
        characterInGame.setImageName(character.getImg());
        characterInGame.setSkills(skill2SkillInGameConverter.convertSkillsWithoutId(Arrays.asList(character.getSkills())));
        return characterInGame;
    }

}
