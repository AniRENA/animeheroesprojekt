package anime.heroes.projekt.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class SearchEnemy {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;

    @Column(name = "player_one")
    private String playerOne;

    @Column(name = "player_two")
    private String playerTwo;

    @Version
    private Integer version;
}
