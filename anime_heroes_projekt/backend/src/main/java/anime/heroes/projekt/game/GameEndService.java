package anime.heroes.projekt.game;

import anime.heroes.projekt.entity.GameUser;
import anime.heroes.projekt.entity.json.gameState.State;
import anime.heroes.projekt.repository.GameStateRepository;
import anime.heroes.projekt.repository.GameUserRepository;
import org.springframework.stereotype.Service;

@Service
public class GameEndService {

    private final GameUserRepository gameUserRepository;
    private final GameStateRepository gameStateRepository;

    public GameEndService(GameUserRepository gameUserRepository, GameStateRepository gameStateRepository) {
        this.gameUserRepository = gameUserRepository;
        this.gameStateRepository = gameStateRepository;
    }

    public boolean isGameOver(State state){
        return isPlayerOneLoose(state) || isPlayerTwoLoose(state);
    }

    public void refreshDatabaseWithGameResult(State state){
        GameUser gameUserOne = gameUserRepository.findByUserName(state.getPlayerOneName());
        GameUser gameUserTwo = gameUserRepository.findByUserName(state.getPlayerTwoName());

        if(isPlayerOneLoose(state)){
            gameUserTwo.setWins(gameUserTwo.getWins() + 1);
            gameUserRepository.save(gameUserTwo);

            gameUserOne.setLoses(gameUserOne.getLoses() + 1);
            gameUserRepository.save(gameUserOne);
        } else if(isPlayerTwoLoose(state)){
            gameUserOne.setWins(gameUserOne.getWins() + 1);
            gameUserRepository.save(gameUserOne);

            gameUserTwo.setLoses(gameUserTwo.getLoses() + 1);
            gameUserRepository.save(gameUserTwo);
        } else{
            throw new RuntimeException("Called this method, but the game is not over!!!");
        }

        gameStateRepository.delete(gameStateRepository.getByPlayerOneAndPlayerTwo(state.getPlayerOneName()));
    }

    private boolean isPlayerOneLoose(State state){
        return state.getPlayerOneCharacters().stream().filter(player -> player.getActualHealth() <=0).count() == state.getPlayerOneCharacters().size();
    }

    private boolean isPlayerTwoLoose(State state){
        return state.getPlayerTwoCharacters().stream().filter(player -> player.getActualHealth() <=0).count() == state.getPlayerTwoCharacters().size();
    }

}
