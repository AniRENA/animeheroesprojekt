package anime.heroes.projekt.game.effect;

import anime.heroes.projekt.changeTurn.dto.TurnChangeCharacter;
import anime.heroes.projekt.selection.searchEnemy.dto.fromJson.Effect;

import java.util.List;

public abstract class EffectHandlerBase {

    public abstract void calculateEffect(List<TurnChangeCharacter> charactersInGameState, List<TurnChangeCharacter> enemyCharactersInGameState, Effect effect, int target);

}
