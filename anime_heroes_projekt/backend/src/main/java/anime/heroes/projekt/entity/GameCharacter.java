package anime.heroes.projekt.entity;

import anime.heroes.projekt.common.CharacterName;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

@Data
@Entity
public class GameCharacter {

    @Id
    private int id;

    @Enumerated(EnumType.STRING)
    private CharacterName name;

}
