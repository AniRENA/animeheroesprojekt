package anime.heroes.projekt;

import anime.heroes.projekt.authentication.AuthenticationProvider;
import anime.heroes.projekt.common.CharacterName;
import anime.heroes.projekt.entity.SearchEnemy;
import anime.heroes.projekt.file.ResourceLoader;
import anime.heroes.projekt.repository.GameUserRepository;
import anime.heroes.projekt.repository.SearchEnemyRepository;
import anime.heroes.projekt.selection.searchEnemy.dto.fromJson.Character;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@Slf4j
@RestController
@ConfigurationProperties(prefix="taco.orders")
@RequestMapping(path="/api", produces="application/json")
public class TestAPIEndpoint {

    @Autowired
    private GameUserRepository gameUserRepository;

    @Autowired
    private AuthenticationProvider authenticationProvider;

    @Autowired
    private SearchEnemyRepository searchEnemyRepository;

    @Autowired
    private SearchEnemyDBService searchEnemyService;

    @GetMapping("korte")
    public String korte() {
        String match = asd();
        if(match == null){
            return "null";
        }
        return match;
    }

    private String asd() {
        int i = 0;
        boolean inserted = false;
        while (true) {
            i++;
            if(i<15){
                SearchEnemy ene = searchEnemyService.search();
                if(ene != null){
                    return ene.getPlayerTwo() + " " + ene.getPlayerOne();
                }
            }else{
                if(!inserted){
                    SearchEnemy ss = SearchEnemy.builder().playerOne(authenticationProvider.getAuthentication().getName()).build();
                    searchEnemyService.saveInNewTransaction(ss);
                    inserted = true;
                }
                SearchEnemy maybe = searchEnemyService.checkHasPairWhileWaiting(authenticationProvider.getAuthentication().getName());
                if(maybe != null){
                    String asd = maybe.getPlayerTwo() + " " + maybe.getPlayerOne();
                    searchEnemyRepository.delete(maybe);
                    return asd;
                }
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @GetMapping("/alma")
    public String home() {
        log.info("authorized user: " + authenticationProvider.getAuthentication().getName());
        String name = authenticationProvider.getAuthentication().getName();
        ResourceLoader resourceLoader = new ResourceLoader();
        try {
            Character character = new Gson().fromJson(resourceLoader.readCharacterAsJSON(CharacterName.ZORO), Character.class);
            log.info(character.toString());
            //log.info(gameUserRepository.findById(1).get().toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "alma";
    }

}
