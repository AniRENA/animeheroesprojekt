package anime.heroes.projekt.repository;

import anime.heroes.projekt.entity.GameUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface GameUserRepository extends JpaRepository<GameUser, Integer> {

    @Query("SELECT u FROM GameUser u WHERE u.userName = ?1")
    GameUser findByUserName(String userName);
}
