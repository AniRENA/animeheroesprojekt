package anime.heroes.projekt.game.cost;

import org.springframework.stereotype.Service;

import java.util.Random;

@Service
class RandomIntegerGeneratorService {

    private Random random = new Random();

    int getRandomNumberInRange(int min, int max) {
        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        return random.nextInt((max - min) + 1) + min;
    }

}
