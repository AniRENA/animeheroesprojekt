package anime.heroes.projekt.game.effect.executors.attack;

import anime.heroes.projekt.changeTurn.dto.TurnChangeCharacter;
import anime.heroes.projekt.game.effect.EffectHandler;
import anime.heroes.projekt.game.effect.EffectHandlerBase;
import anime.heroes.projekt.selection.searchEnemy.dto.fromJson.Effect;
import anime.heroes.projekt.selection.searchEnemy.dto.fromJson.EffectType;
import anime.heroes.projekt.selection.searchEnemy.dto.fromJson.TargetsOfSkill;

import java.util.List;

@EffectHandler(EffectType.ATTACK_PHYSICAL)
public class AttackPhysicalHandler extends EffectHandlerBase {

    @Override
    public void calculateEffect(List<TurnChangeCharacter> charactersInGameState, List<TurnChangeCharacter> enemyCharactersInGameState, Effect effect, int target) {
        TurnChangeCharacter targetCharacter = enemyCharactersInGameState.get(target - 3);
        if (effect.getEffected() == TargetsOfSkill.ALL_ENEMIES) {
            enemyCharactersInGameState.forEach(enemyCharacter -> enemyCharacter.setActualHealth(enemyCharacter.getActualHealth() - effect.getValue()));
        } else if (effect.getEffected() == TargetsOfSkill.ENEMY) {
            targetCharacter.setActualHealth(targetCharacter.getActualHealth() - effect.getValue());
        } else {
            //TODO Valami hack exceptiont kellene dobni
            throw new RuntimeException("Hack");
        }
    }
}
