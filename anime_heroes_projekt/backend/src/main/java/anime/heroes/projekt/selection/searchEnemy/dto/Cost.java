package anime.heroes.projekt.selection.searchEnemy.dto;

public enum Cost {
    ability(0),
    power(1),
    tactic(2),
    tool(3),
    random(-1)
    ;

    private final int orderValue;

    Cost(int orderValue) {
        this.orderValue = orderValue;
    }

    public int getOrderValue() {
        return orderValue;
    }
}
