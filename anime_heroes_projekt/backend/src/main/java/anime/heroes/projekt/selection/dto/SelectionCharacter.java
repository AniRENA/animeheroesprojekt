package anime.heroes.projekt.selection.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SelectionCharacter {
    private String name;
    private String img;
    private boolean owned;
    private List<SelectionItem> items;
}
