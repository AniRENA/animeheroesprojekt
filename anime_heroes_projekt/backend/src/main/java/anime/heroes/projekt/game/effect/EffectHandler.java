package anime.heroes.projekt.game.effect;

import anime.heroes.projekt.selection.searchEnemy.dto.fromJson.EffectType;
import org.springframework.context.annotation.Scope;
import org.springframework.core.annotation.AliasFor;
import org.springframework.stereotype.Component;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Component
@Scope("prototype")
public @interface EffectHandler {

    EffectType value();

    @AliasFor(annotation = Component.class, attribute = "value") String component() default "";

    @AliasFor(annotation = Scope.class, attribute = "value") String scope() default "prototype";

}
