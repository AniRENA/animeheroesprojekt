package anime.heroes.projekt.changeTurn.dto;

import anime.heroes.projekt.selection.searchEnemy.dto.ItemInGame;
import anime.heroes.projekt.selection.searchEnemy.dto.fromJson.Skill;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class TurnChangeCharacter {

    private String name;
    private String imageName;
    private int health;
    private int actualHealth;
    private ItemInGame item;
    private List<ChangeTurnSkill> buffs; /*ebbe a buffok vannak rajta*/
    private List<Skill> skills;
    private List<Skill> altSkills;

}
