package anime.heroes.projekt.selection.searchEnemy.converter.fromJson;

import anime.heroes.projekt.selection.searchEnemy.dto.Cost;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class Cost2SkillInGameCostConverter {

    public List<Cost> convert(List<anime.heroes.projekt.selection.searchEnemy.dto.fromJson.Cost> costs){
        List<Cost> result = new ArrayList<>();
        for(anime.heroes.projekt.selection.searchEnemy.dto.fromJson.Cost cost : costs){
            for(int i=0;i<cost.getCount();i++){
                result.add(Cost.valueOf(cost.getName()));
            }
        }
        return result;
    }

}
