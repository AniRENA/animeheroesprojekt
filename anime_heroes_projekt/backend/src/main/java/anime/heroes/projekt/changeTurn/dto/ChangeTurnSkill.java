package anime.heroes.projekt.changeTurn.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ChangeTurnSkill {

    private int id;
    private String name;
    private String description;
    private String imageName;
    private String[] classes;
    private String[] costs;
    private String state;
    private int[] whoCanBeATarget;
    private int whoIsTheTarget;
    private int cooldown;/*Ha ez nulla, akkor nincs cd, mivel ezt a kliensnek küldi, és ő azt jeleníti meg, hogy mennyi cd van rajta, és ha nulla, akkor nincs cd.*/

}
