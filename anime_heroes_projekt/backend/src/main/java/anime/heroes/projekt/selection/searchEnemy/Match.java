package anime.heroes.projekt.selection.searchEnemy;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Match {
    private String player1;
    private String player2;
}
