package anime.heroes.projekt.repository;

import anime.heroes.projekt.entity.GameState;
import anime.heroes.projekt.entity.GameStateId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface GameStateRepository extends JpaRepository<GameState, GameStateId> {

    @Query("SELECT distinct state FROM GameState state WHERE gameStateId.playerOne = ?1 OR gameStateId.playerTwo = ?1")
    GameState getByPlayerOneAndPlayerTwo(String player);

}
