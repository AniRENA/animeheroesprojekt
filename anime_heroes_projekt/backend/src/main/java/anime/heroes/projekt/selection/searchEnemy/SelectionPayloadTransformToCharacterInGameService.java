package anime.heroes.projekt.selection.searchEnemy;

import anime.heroes.projekt.common.CharacterName;
import anime.heroes.projekt.common.ItemName;
import anime.heroes.projekt.file.ResourceLoader;
import anime.heroes.projekt.selection.dto.SelectionCharacter;
import anime.heroes.projekt.selection.dto.SelectionItem;
import anime.heroes.projekt.selection.dto.SelectionPayload;
import anime.heroes.projekt.selection.items.Item;
import anime.heroes.projekt.selection.searchEnemy.converter.fromJson.Character2CharacterInGameConverter;
import anime.heroes.projekt.selection.searchEnemy.converter.fromJson.Item2ItemInGameConverter;
import anime.heroes.projekt.selection.searchEnemy.converter.fromJson.Skill2SkillInGameConverter;
import anime.heroes.projekt.selection.searchEnemy.dto.*;
import anime.heroes.projekt.selection.searchEnemy.dto.Class;
import anime.heroes.projekt.selection.searchEnemy.dto.fromJson.Character;
import anime.heroes.projekt.selection.searchEnemy.dto.fromJson.Skill;
import com.google.gson.Gson;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class SelectionPayloadTransformToCharacterInGameService {

    private final Character2CharacterInGameConverter character2CharacterInGameConverter;
    private final Skill2SkillInGameConverter skill2SkillInGameConverter;
    private final Item2ItemInGameConverter item2ItemInGameConverter;

    public SelectionPayloadTransformToCharacterInGameService(Character2CharacterInGameConverter character2CharacterInGameConverter, Skill2SkillInGameConverter skill2SkillInGameConverter, Item2ItemInGameConverter item2ItemInGameConverter) {
        this.character2CharacterInGameConverter = character2CharacterInGameConverter;
        this.skill2SkillInGameConverter = skill2SkillInGameConverter;
        this.item2ItemInGameConverter = item2ItemInGameConverter;
    }

    List<CharacterInGame> createCharacterInGames(SelectionPayload selectionPayload) {
        List<CharacterName> names = selectionPayload.getCharacters().stream()
                .map(character -> CharacterName.valueOf(character
                        .getName()
                        .toUpperCase()))
                .collect(Collectors.toList());
        List<CharacterInGame> result = getUserCharacters(names);

        List<SkillInGame> activeItems = getUserActiveItems(getItemNames(selectionPayload, SelectionItem::isIsActive));

        result.get(0).getSkills().add(0, activeItems.get(0));
        result.get(1).getSkills().add(0, activeItems.get(1));
        result.get(2).getSkills().add(0, activeItems.get(2));

        setPassiveItemsForCharacters(result, selectionPayload);

        return result;
    }

    private List<CharacterInGame> getUserCharacters(List<CharacterName> characters) {
        ResourceLoader resourceLoader = new ResourceLoader();
        try {
            Character character1 = new Gson().fromJson(resourceLoader.readCharacterAsJSON(characters.get(0)), Character.class);
            Character character2 = new Gson().fromJson(resourceLoader.readCharacterAsJSON(characters.get(1)), Character.class);
            Character character3 = new Gson().fromJson(resourceLoader.readCharacterAsJSON(characters.get(2)), Character.class);
            return Arrays.asList(
                    character2CharacterInGameConverter.convertWithoutItems(character1),
                    character2CharacterInGameConverter.convertWithoutItems(character2),
                    character2CharacterInGameConverter.convertWithoutItems(character3)
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private List<SkillInGame> getUserActiveItems(List<ItemName> itemNames) {
        ResourceLoader resourceLoader = new ResourceLoader();
        try {
            Skill activeItem1 = new Gson().fromJson(resourceLoader.readActiveItemAsSkillAsJson(itemNames.get(0)), Skill.class);
            Skill activeItem2 = new Gson().fromJson(resourceLoader.readActiveItemAsSkillAsJson(itemNames.get(1)), Skill.class);
            Skill activeItem3 = new Gson().fromJson(resourceLoader.readActiveItemAsSkillAsJson(itemNames.get(2)), Skill.class);
            return Arrays.asList(
                    skill2SkillInGameConverter.convertSkillWithoutId(activeItem1),
                    skill2SkillInGameConverter.convertSkillWithoutId(activeItem2),
                    skill2SkillInGameConverter.convertSkillWithoutId(activeItem3)
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private List<ItemInGame> getPassiveItems(List<ItemName> itemNames){
        ResourceLoader resourceLoader = new ResourceLoader();
        try {
            Item item1 = new Gson().fromJson(resourceLoader.readItemAsJSON(itemNames.get(0)), Item.class);
            Item item2 = new Gson().fromJson(resourceLoader.readItemAsJSON(itemNames.get(1)), Item.class);
            Item item3 = new Gson().fromJson(resourceLoader.readItemAsJSON(itemNames.get(2)), Item.class);
            return Arrays.asList(
                    item2ItemInGameConverter.convert(item1),
                    item2ItemInGameConverter.convert(item2),
                    item2ItemInGameConverter.convert(item3)
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private List<ItemName> getItemNames(SelectionPayload selectionPayload, Predicate<SelectionItem> predicate){
        List<ItemName> result = new ArrayList<>();
        for(SelectionCharacter character : selectionPayload.getCharacters()){
            result.add(Stream.of(ItemName.values())
                    .filter(i -> i.getValue().equals(
                            character.getItems().stream()
                                .filter(predicate::test)
                                .collect(Collectors.toList())
                                    .get(0)
                                    .getName()))
                    .collect(Collectors.toList()).get(0));
        }
        return result;
    }

    private void setPassiveItemsForCharacters(List<CharacterInGame> characters, SelectionPayload selectionPayload) {
        List<ItemInGame> items = getPassiveItems(getItemNames(selectionPayload, item -> !item.isIsActive()));
        AtomicInteger index = new AtomicInteger();
        characters.forEach(character -> character.setItem(items.get(index.getAndIncrement())));
    }

}
