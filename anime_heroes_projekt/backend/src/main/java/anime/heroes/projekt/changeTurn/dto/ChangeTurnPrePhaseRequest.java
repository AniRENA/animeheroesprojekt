package anime.heroes.projekt.changeTurn.dto;

import lombok.Data;

import java.util.List;

@Data
public class ChangeTurnPrePhaseRequest {

    private List<ChangeTurnSkill> usedSkills;

}
