package anime.heroes.projekt.selection.searchEnemy.dto;

import lombok.Data;

import java.util.List;

@Data
public class CharacterInGame {
    private String name;
    private String imageName;
    private int health;
    private int actualHealth;
    private List<SkillInGame> skills;
    private ItemInGame item;
}
