package anime.heroes.projekt.selection.searchEnemy.dto;

import anime.heroes.projekt.selection.searchEnemy.dto.user.UserInfo;
import lombok.Data;

import java.util.List;

@Data
public class SearchEnemy {
    private boolean find;
    private boolean startsTheGame;
    private UserInfo enemyUser;
    private List<CharacterInGame> enemyCharacters;
    private UserInfo user;
    private List<CharacterInGame> characters;
    private List<CostWithValue> costs;
}
