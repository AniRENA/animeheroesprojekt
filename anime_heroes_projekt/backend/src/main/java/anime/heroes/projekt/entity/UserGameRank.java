package anime.heroes.projekt.entity;

public enum UserGameRank {
    GHOUL("GHOUL"),
    NEWBIE_WARRIOR("NEWBIE_WARRIOR"),
    HOBBY_HEROE("HOBBY HEROE"),
    EXORCIST("EXORCIST"),
    REAPER("REAPER"),
    AIRBENDER("AIRBENDER"),
    STRAW_HAT_PIRATE("STRAW_HAT_PIRATE")
    ;

    private final String value;

    UserGameRank(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
