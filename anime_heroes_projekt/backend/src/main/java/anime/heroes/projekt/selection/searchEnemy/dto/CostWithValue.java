package anime.heroes.projekt.selection.searchEnemy.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class CostWithValue {
    private Cost name;
    private int value;
}
