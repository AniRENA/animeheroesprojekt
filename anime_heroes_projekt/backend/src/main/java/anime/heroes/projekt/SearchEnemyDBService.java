package anime.heroes.projekt;

import anime.heroes.projekt.authentication.AuthenticationProvider;
import anime.heroes.projekt.entity.SearchEnemy;
import anime.heroes.projekt.repository.SearchEnemyRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

@Slf4j
@Service
public class SearchEnemyDBService {

    @Autowired
    private AuthenticationProvider authenticationProvider;

    @Autowired
    private SearchEnemyRepository searchEnemyRepository;

    @Autowired
    private EntityManager entityManager;

    public SearchEnemy search() {
        List<SearchEnemy> result = searchEnemyRepository.findByPlayerOneNullOrPlayerTwoNull();
        if (result != null && !result.isEmpty()) {
            SearchEnemy searchEnemy = result.get(0);
            try{
                if (searchEnemy.getPlayerOne() == null) {
                    searchEnemy.setPlayerOne(authenticationProvider.getAuthentication().getName());
                    return searchEnemyRepository.save(searchEnemy);
                } else {
                    searchEnemy.setPlayerTwo(authenticationProvider.getAuthentication().getName());
                    return searchEnemyRepository.save(searchEnemy);
                }
            }catch(Exception e){
                log.info(e.toString());
            }
        }
        return null;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public SearchEnemy checkHasPairWhileWaiting(String username) {
        SearchEnemy s = searchEnemyRepository.findByUsername(username);
        if (s != null) {
            entityManager.refresh(s);
        }
        return s;
    }

    public SearchEnemy saveInNewTransaction(SearchEnemy searchEnemy){
        return searchEnemyRepository.save(searchEnemy);
    }

}
