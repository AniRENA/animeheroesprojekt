package anime.heroes.projekt.selection.searchEnemy;

import anime.heroes.projekt.entity.GameUser;
import anime.heroes.projekt.repository.GameUserRepository;
import anime.heroes.projekt.selection.searchEnemy.converter.fromJson.GameUser2UserInfoConverter;
import anime.heroes.projekt.selection.searchEnemy.dto.user.UserInfo;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private final GameUserRepository gameUserRepository;
    private final GameUser2UserInfoConverter gameUser2UserInfoConverter;

    public UserService(GameUserRepository gameUserRepository, GameUser2UserInfoConverter gameUser2UserInfoConverter) {
        this.gameUserRepository = gameUserRepository;
        this.gameUser2UserInfoConverter = gameUser2UserInfoConverter;
    }

    public UserInfo createUserInfoFromUserName(String userName){
        return gameUser2UserInfoConverter.convert(gameUserRepository.findByUserName(userName));
    }
}
