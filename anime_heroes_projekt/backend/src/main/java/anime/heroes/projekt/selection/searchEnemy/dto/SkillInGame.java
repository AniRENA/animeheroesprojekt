package anime.heroes.projekt.selection.searchEnemy.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder
@Data
public class SkillInGame {
    private int id;
    private String name;
    private String description;
    private String imageName;
    private List<Class> classes;
    private List<Cost> costs;
    private State state;
    private List<Integer> whoCanBeATarget;
    private int cooldown;
    private int actualCooldown;
}
