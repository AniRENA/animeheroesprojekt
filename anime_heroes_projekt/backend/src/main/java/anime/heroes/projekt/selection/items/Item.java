package anime.heroes.projekt.selection.items;

import lombok.*;

@Data
@Builder
public class Item {
    private String name;
    private String img;
    private boolean owned;
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private boolean isActive;

    public boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(boolean active) {
        isActive = active;
    }
}
