package anime.heroes.projekt.selection.profile;

import anime.heroes.projekt.authentication.AuthenticationProvider;
import anime.heroes.projekt.entity.GameUser;
import anime.heroes.projekt.repository.GameUserRepository;
import anime.heroes.projekt.selection.profile.dto.SelectionProfile;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping(path="/api", produces="application/json")
public class SelectionProfileEndpoint {

    private final AuthenticationProvider authenticationProvider;
    private final GameUserRepository gameUserRepository;

    public SelectionProfileEndpoint(AuthenticationProvider authenticationProvider, GameUserRepository gameUserRepository) {
        this.authenticationProvider = authenticationProvider;
        this.gameUserRepository = gameUserRepository;
    }

    @GetMapping("/selectionProfile")
    public SelectionProfile getAllItems() {
        String actualUser = authenticationProvider.getAuthentication().getName();
        GameUser gameUser = gameUserRepository.findByUserName(actualUser);
        return SelectionProfile.builder()
                .userName(gameUser.getUserName())
                .rank(gameUser.getRank().getValue())
                .clan(gameUser.getClan())
                .level(gameUser.getLevel())
                .ladderRank(gameUser.getLadderRank())
                .wins(gameUser.getWins())
                .loses(gameUser.getLoses())
                .ratio(gameUser.getRatio()).build();
    }
}
