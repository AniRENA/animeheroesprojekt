package anime.heroes.projekt.selection.searchEnemy.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ItemInGame {
    private String name;
    private boolean isActive;
    private String imageName;
    private String description;
}
