package anime.heroes.projekt.entity.json.gameState;

import anime.heroes.projekt.changeTurn.dto.TurnChangeCharacter;
import anime.heroes.projekt.selection.searchEnemy.dto.CostWithValue;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class State {

    private String playerOneName;
    private String playerTwoName;
    private List<TurnChangeCharacter> playerOneCharacters;/*A játékot a playerOne kezdi.*/
    private List<TurnChangeCharacter> playerTwoCharacters;
    private List<CostWithValue> playerOneCosts;
    private List<CostWithValue> playerTwoCosts;

}
