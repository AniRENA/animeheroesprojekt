package anime.heroes.projekt.common;

public enum ItemName {
    AMONS_MASK("AmonsMaskItem"),
    BLAZE_SWORD("BlazeSwordItem"),
    BLOOD_SWORD("BloodSwordItem"),
    DEMON_SWORD("DemonSwordItem"),
    GOLDEN_EYE("GoldenEyeItem"),
    LIGHT_SABER("LightSaberItem"),
    MACHINE_GUN("MachineGunItem"),
    MAKE_UP_SET("MakeUpSetItem"),
    MOTOR_BIKE("MotorBikeItem"),
    ;

    private final String value;

    ItemName(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
