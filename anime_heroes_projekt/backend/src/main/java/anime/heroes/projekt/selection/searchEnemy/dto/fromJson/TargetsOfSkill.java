package anime.heroes.projekt.selection.searchEnemy.dto.fromJson;

public enum TargetsOfSkill {
    SELF, /*Csak magára rakhatja*/
    FRIENDS, /*Csak a barátokra rakhatja*/
    ALL_FRIENDLY, /*Mindhárom barátra rakhatja*/
    FRIEND, /*Csak egy barátra rakhatja*/
    ENEMY, /*Csak egy ellenségre rakhatja*/
    ALL_ENEMIES, /*Az összes ellenségre rakhatja*/
    ALL /*Mindenki aki a pályán van*/
}
