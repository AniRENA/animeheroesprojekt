package anime.heroes.projekt.changeTurn;

import anime.heroes.projekt.authentication.AuthenticationProvider;
import anime.heroes.projekt.changeTurn.dto.ChangeTurnPrePhaseRequest;
import anime.heroes.projekt.changeTurn.dto.TurnChangeCharacter;
import anime.heroes.projekt.changeTurn.dto.TurnChangeResponse;
import anime.heroes.projekt.entity.GameState;
import anime.heroes.projekt.entity.json.gameState.State;
import anime.heroes.projekt.repository.GameStateRepository;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping(path="/api", produces="application/json")
public class TurnChangePrePhaseEndpoint {

    private final GameStateRepository gameStateRepository;
    private final AuthenticationProvider authenticationProvider;
    private final TurnChangePrePhaseService turnChangePrePhaseService;

    public TurnChangePrePhaseEndpoint(GameStateRepository gameStateRepository,
                                      AuthenticationProvider authenticationProvider,
                                      TurnChangePrePhaseService turnChangePrePhaseService) {
        this.gameStateRepository = gameStateRepository;
        this.authenticationProvider = authenticationProvider;
        this.turnChangePrePhaseService = turnChangePrePhaseService;
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PostMapping("/turnChangePrePhase")
    public TurnChangeResponse changeTurnPrePhase(@RequestBody ChangeTurnPrePhaseRequest changeTurnPrePhaseRequest){
        String userName = authenticationProvider.getAuthentication().getName();
        GameState gameState = gameStateRepository.getByPlayerOneAndPlayerTwo(userName);

        State state = new Gson().fromJson(gameState.getState(), State.class);

        List<TurnChangeCharacter> characters;
        List<TurnChangeCharacter> enemyCharacters;

        if(state.getPlayerOneName().equals(userName)) {
            characters = state.getPlayerOneCharacters();
            enemyCharacters = state.getPlayerTwoCharacters();
        } else {
            characters = state.getPlayerTwoCharacters();
            enemyCharacters = state.getPlayerOneCharacters();
        }

        turnChangePrePhaseService.calculateUsedSkillsConsequences(characters, enemyCharacters, changeTurnPrePhaseRequest.getUsedSkills());

        gameState.setState(new Gson().toJson(state));

        gameStateRepository.save(gameState);

        return TurnChangeResponse.builder()
                .characters(characters)
                .enemyCharacters(enemyCharacters).build();
    }

}
