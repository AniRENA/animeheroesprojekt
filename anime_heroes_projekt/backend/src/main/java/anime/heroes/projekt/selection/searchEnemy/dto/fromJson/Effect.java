package anime.heroes.projekt.selection.searchEnemy.dto.fromJson;

import lombok.Data;

@Data
public class Effect {
    private int round;
    private EffectType type;
    private int value;
    private TargetsOfSkill effected;
}
