package anime.heroes.projekt.selection.searchEnemy.dto.fromJson;

import lombok.Data;

@Data
public class Cost {
    private String name;
    private int count;
}
