package anime.heroes.projekt.util.polling;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Polling<T> {

    private final PollingAction<T> action;
    private int maxCounter;
    private int sleepTime = 1000;
    private PollingInterrupt interruptChecker = () -> Thread.currentThread().isInterrupted();

    public Polling(int maxCounter, PollingAction<T> action) {
        this.maxCounter = maxCounter;
        this.action = action;
    }

    public T poll() {
        int counter = 0;

        while (!interruptChecker.isInterrupted()) {
            T data = action.run();

            if (data != null) {
                return data;
            }

            counter++;

            if (counter == maxCounter) {
                log.error("Earn max counter:" + counter + " and return null!");

                return null;
            }

            try {
                Thread.sleep(sleepTime);
            } catch (InterruptedException ignore) {
            }
        }

        log.error("Cant find element and return null!");

        return null;
    }

    public PollingAction<T> getAction() {
        return action;
    }

    public int getMaxCounter() {
        return maxCounter;
    }

    public Polling<T> setMaxCounter(int maxCounter) {
        this.maxCounter = maxCounter;
        return this;
    }

    public int getSleepTime() {
        return sleepTime;
    }

    public Polling<T> setSleepTime(int sleepTime) {
        this.sleepTime = sleepTime;
        return this;
    }

    public PollingInterrupt getInterruptChecker() {
        return interruptChecker;
    }

    public Polling<T> setInterruptChecker(PollingInterrupt interruptChecker) {
        this.interruptChecker = interruptChecker;
        return this;
    }

}
