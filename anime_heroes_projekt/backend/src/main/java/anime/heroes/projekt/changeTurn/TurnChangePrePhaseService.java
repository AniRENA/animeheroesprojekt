package anime.heroes.projekt.changeTurn;

import anime.heroes.projekt.changeTurn.dto.ChangeTurnSkill;
import anime.heroes.projekt.changeTurn.dto.TurnChangeCharacter;
import anime.heroes.projekt.common.CharacterName;
import anime.heroes.projekt.file.ResourceLoader;
import anime.heroes.projekt.game.effect.EffectHandlerExecutorService;
import anime.heroes.projekt.selection.searchEnemy.dto.fromJson.Character;
import anime.heroes.projekt.selection.searchEnemy.dto.fromJson.Effect;
import anime.heroes.projekt.selection.searchEnemy.dto.fromJson.EffectType;
import anime.heroes.projekt.selection.searchEnemy.dto.fromJson.Skill;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
class TurnChangePrePhaseService {

    private final CdCalculateService cdCalculateService;
    private final EffectHandlerExecutorService effectHandlerExecutorService;

    public TurnChangePrePhaseService(CdCalculateService cdCalculateService, EffectHandlerExecutorService effectHandlerExecutorService) {
        this.cdCalculateService = cdCalculateService;
        this.effectHandlerExecutorService = effectHandlerExecutorService;
    }

    void calculateUsedSkillsConsequences(List<TurnChangeCharacter> charactersInGameState, List<TurnChangeCharacter> enemyCharactersInGameState,
                                         List<ChangeTurnSkill> usedSkills){
        ResourceLoader resourceLoader = new ResourceLoader();
        List<Character> characters = new ArrayList<>();
        //TODO az ellenfél skilljei védekező hatásai innen fognak majd kiderülni!
        List<Character> enemyCharacters = new ArrayList<>();

        charactersInGameState.stream().map(TurnChangeCharacter::getName).forEach(name -> {
            try {
                characters.add(new Gson().fromJson(resourceLoader.readCharacterAsJSON(CharacterName.valueOf(name.toUpperCase())), Character.class));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        enemyCharactersInGameState.stream().map(TurnChangeCharacter::getName).forEach(name ->{
            try {
                enemyCharacters.add(new Gson().fromJson(resourceLoader.readCharacterAsJSON(CharacterName.valueOf(name.toUpperCase())), Character.class));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        usedSkills.forEach(usedSkill -> {
            Skill skill = convertChangeTurnSkillToCharacterSkill(characters, usedSkill);
            cdCalculateService.actualizeCd(charactersInGameState, skill);
            int effectedValue = skill.getEffects()[0].getValue();

            /*TODO ezt az executeot kell majd felokosítani, és meghívni, minden egyes effect miatt!!!!!!*/
            effectHandlerExecutorService.execute(charactersInGameState, enemyCharactersInGameState, skill.getEffects()[0], usedSkill.getWhoIsTheTarget());
            /*enemyCharactersInGameState.forEach(enemyCharacter -> {
                enemyCharacter.setActualHealth(enemyCharacter.getActualHealth() - effectedValue);
            });*/
        });

        cdCalculateService.decreaseCd(charactersInGameState, usedSkills);
    }

    private Skill convertChangeTurnSkillToCharacterSkill(List<Character> characters, ChangeTurnSkill changeTurnSkill){
        for(Character character : characters){
            for(Skill skill : character.getSkills()){
                if(skill.getName().equals(changeTurnSkill.getName())){
                    return skill;
                }
            }
        }
        throw new RuntimeException("Cannot find the skill!");
    }

}
