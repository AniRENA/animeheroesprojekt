package anime.heroes.projekt.selection.searchEnemy.converter.fromJson;

import anime.heroes.projekt.selection.searchEnemy.dto.SkillInGame;
import anime.heroes.projekt.selection.searchEnemy.dto.State;
import anime.heroes.projekt.selection.searchEnemy.dto.fromJson.Skill;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class Skill2SkillInGameConverter {

    private Cost2SkillInGameCostConverter cost2SkillInGameCostConverter;

    public Skill2SkillInGameConverter(Cost2SkillInGameCostConverter cost2SkillInGameCostConverter) {
        this.cost2SkillInGameCostConverter = cost2SkillInGameCostConverter;
    }

    List<SkillInGame> convertSkillsWithoutId(List<Skill> skills) {
        return skills.stream().map(this::convertSkillWithoutId).collect(Collectors.toList());
    }

    public SkillInGame convertSkillWithoutId(Skill skill) {
        return SkillInGame.builder()
                .name(skill.getName())
                .description(skill.getDescription())
                .imageName(skill.getImg())
                .classes(skill.getClasses())
                .costs(cost2SkillInGameCostConverter.convert(Arrays.asList(skill.getCost())))
                .state(State.NOTHING)
                .whoCanBeATarget(Arrays.asList(0, 1, 2, 3, 4, 5))/*TODO ez sincs fájlba átrakva még az információ,
                 ami alapján ezt számolhatjuk*/
                .cooldown(skill.getCd())
                .actualCooldown(skill.getActualCd())
                .build();
    }

}
