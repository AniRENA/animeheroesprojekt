package anime.heroes.projekt.repository;

import anime.heroes.projekt.entity.SearchEnemy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SearchEnemyRepository extends JpaRepository<SearchEnemy, Integer> {

    @Query("SELECT search FROM SearchEnemy search WHERE (search.playerOne IS NULL AND search.playerTwo IS NOT NULL ) " +
            "OR (search.playerOne IS NOT NULL AND search.playerTwo IS NULL)")
    List<SearchEnemy> findByPlayerOneNullOrPlayerTwoNull();

    @Query("SELECT search FROM SearchEnemy search WHERE (search.playerOne = ?1 AND search.playerTwo IS NOT NULL ) " +
            "OR (search.playerTwo = ?1 AND search.playerOne IS NOT NULL)")
    SearchEnemy findByUsername(String username);

}
