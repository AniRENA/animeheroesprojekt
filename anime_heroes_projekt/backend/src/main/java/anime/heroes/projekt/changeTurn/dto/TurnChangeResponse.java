package anime.heroes.projekt.changeTurn.dto;

import anime.heroes.projekt.selection.searchEnemy.dto.CostWithValue;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class TurnChangeResponse {

    private List<TurnChangeCharacter> enemyCharacters;
    private List<TurnChangeCharacter> characters;
    private List<CostWithValue> costs; /*Ebből ugye azért csak egy van, mert ez az objektum annak megy vissza, aki megkapja a kört.*/

}
