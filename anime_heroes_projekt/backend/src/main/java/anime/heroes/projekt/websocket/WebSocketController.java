package anime.heroes.projekt.websocket;

import anime.heroes.projekt.changeTurn.dto.TurnChangeCharacter;
import anime.heroes.projekt.changeTurn.dto.TurnChangeResponse;
import anime.heroes.projekt.entity.GameState;
import anime.heroes.projekt.entity.json.gameState.State;
import anime.heroes.projekt.game.GameEndService;
import anime.heroes.projekt.game.cost.CostService;
import anime.heroes.projekt.repository.GameStateRepository;
import anime.heroes.projekt.selection.searchEnemy.dto.CostWithValue;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;

import java.security.Principal;
import java.util.List;

@Slf4j
@Controller
public class WebSocketController {

    private final SimpMessageSendingOperations messagingTemplate;
    private final GameStateRepository gameStateRepository;
    private final GameEndService gameEndService;
    private final CostService costService;
    private final Gson gson = new Gson();

    public WebSocketController(SimpMessageSendingOperations messagingTemplate,
                               GameStateRepository gameStateRepository,
                               GameEndService gameEndService,
                               CostService costService) {
        this.messagingTemplate = messagingTemplate;
        this.gameStateRepository = gameStateRepository;
        this.gameEndService = gameEndService;
        this.costService = costService;
    }

    @MessageMapping("/message")
    @SendToUser("/queue/reply")
    public void processMessageFromClient(@Payload String message, Principal principal) throws Exception {
        String userName = principal.getName();
        GameState gameState = gameStateRepository.getByPlayerOneAndPlayerTwo(userName);
        State state = new Gson().fromJson(gameState.getState(), State.class);
        String enemyUser;

        List<TurnChangeCharacter> characters;
        List<TurnChangeCharacter> enemyCharacters;
        List<CostWithValue> costs;

        if(userName.equals(gameState.getGameStateId().getPlayerOne())){
            enemyUser = gameState.getGameStateId().getPlayerTwo();
            characters = state.getPlayerTwoCharacters();
            enemyCharacters = state.getPlayerOneCharacters();
            costService.generateRandomCostForCostState(state.getPlayerTwoCosts(), 3);
            costs = state.getPlayerTwoCosts();
        } else {
            enemyUser = gameState.getGameStateId().getPlayerOne();
            characters = state.getPlayerOneCharacters();
            enemyCharacters = state.getPlayerTwoCharacters();
            costService.generateRandomCostForCostState(state.getPlayerOneCosts(), 3);
            costs = state.getPlayerOneCosts();
        }

        gameState.setState(gson.toJson(state));

        gameStateRepository.save(gameState);

        if(gameEndService.isGameOver(state)){
            gameEndService.refreshDatabaseWithGameResult(state);
        }

        messagingTemplate.convertAndSendToUser(enemyUser,
                "/queue/reply",
                gson.toJson(TurnChangeResponse.builder()
                        .characters(characters)
                        .enemyCharacters(enemyCharacters)
                        .costs(costs).build()));
    }

    @MessageExceptionHandler
    @SendToUser("/queue/errors")
    public String handleException(Throwable exception) {
        return exception.getMessage();
    }
}
