package anime.heroes.projekt.game.effect;

import anime.heroes.projekt.changeTurn.dto.TurnChangeCharacter;
import anime.heroes.projekt.selection.searchEnemy.dto.fromJson.Effect;
import anime.heroes.projekt.selection.searchEnemy.dto.fromJson.EffectType;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class EffectHandlerExecutorService {

    private final BeanFactory beanFactory;

    private final Map<EffectType, Class<EffectHandlerBase>> effectHandlers = new HashMap<>();


    public EffectHandlerExecutorService(BeanFactory beanFactory) throws ClassNotFoundException{
        this.beanFactory = beanFactory;

        lookupEffectHandlers();
    }

    public void execute(List<TurnChangeCharacter> charactersInGameState, List<TurnChangeCharacter> enemyCharactersInGameState, Effect effect, int target) {
        Class<EffectHandlerBase> handler = effectHandlers.get(effect.getType());
        EffectHandlerBase effectHandler = beanFactory.getBean(handler);
        effectHandler.calculateEffect(charactersInGameState, enemyCharactersInGameState, effect, target);
    }

    @SuppressWarnings("unchecked")
    private void lookupEffectHandlers() throws ClassNotFoundException{
        ClassPathScanningCandidateComponentProvider provider = new ClassPathScanningCandidateComponentProvider(false);
        provider.addIncludeFilter(new AnnotationTypeFilter(EffectHandler.class));

        for (BeanDefinition component : provider.findCandidateComponents("anime/heroes/projekt")) {
            Class<?> baseClass = Class.forName(component.getBeanClassName());
            if (EffectHandlerBase.class.isAssignableFrom(baseClass)) {
                Class<EffectHandlerBase> cls = (Class<EffectHandlerBase>) baseClass;
                EffectHandler effectHandler = cls.getAnnotation(EffectHandler.class);

                if (effectHandlers.containsKey(effectHandler.value())) {
                    throw new IllegalArgumentException("There are multiple effect handlers for type: "
                            + effectHandler.value());
                }

                effectHandlers.put(effectHandler.value(), cls);
            } else {
                throw new IllegalArgumentException(component.getBeanClassName() + " does not extend " +
                        EffectHandlerBase.class.getName());
            }
        }
    }

}
