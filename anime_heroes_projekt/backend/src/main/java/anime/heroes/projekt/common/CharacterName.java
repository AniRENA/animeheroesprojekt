package anime.heroes.projekt.common;

/**
 * Ezt az enumot használjuk például a json fileok név parseolásához.
 * Itt minden karakternek le kell lennie mappelve
 * */
public enum CharacterName {
    AANG("Aang"),
    AKENO("Akeno"),
    ALLEN("Allen"),
    AMI("Ami"),
    KIRITO("Kirito"),
    KATARA("Katara"),
    EDWARD("Edward"),
    ELENA("Elena"),
    LEVI("Levi"),
    ZUKO("Zuko"),
    ASUNA("Asuna"),
    NARUTO("Naruto"),
    LAURA("Laura"),
    GENOS("Genos"),
    INUYASHA("Inuyasha"),
    JELLAL("Jellal"),
    KAZUKI("Kazuki"),
    KIRA("Kira"),
    KORRA("Korra"),
    KURUMI("Kurumi"),
    MAYURI("Mayuri"),
    MUSTANG("Mustang"),
    MIRIA("Miria"),
    RIAS("Rias"),
    RIZE("Rize"),
    SAEKO("Saeko"),
    SAITAMA("Saitama"),
    ICHIGO("Ichigo"),
    SHINJI("Shinji"),
    ARMSTRONG("Armstrong"),
    SCAR("Scar"),
    TERESA("Teresa"),
    TOSHIRO("Toshiro"),
    ERZA("Erza"),
    YUGI("Yugi"),
    YUNO("Yuno"),
    YUUKO("Yuuko"),
    DOFLAMINGO("Doflamingo"),
    ZORO("Zoro"),
    ;

    private final String value;

    CharacterName(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
