package anime.heroes.projekt;

import anime.heroes.projekt.entity.GameUser;
import anime.heroes.projekt.repository.GameUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class GameUserDetailsService implements UserDetailsService {

    @Autowired
    private GameUserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) {
        GameUser user = userRepository.findByUserName(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }
        return new GameUserPrincipal(user);
    }
}

