package anime.heroes.projekt.registration;

import anime.heroes.projekt.entity.GameUser;
import anime.heroes.projekt.entity.UserGameRank;
import anime.heroes.projekt.registration.dto.RegistrationDTO;
import anime.heroes.projekt.registration.service.RegistrationValidatorService;
import anime.heroes.projekt.repository.GameUserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@Slf4j
@RestController
@RequestMapping(path = "/api", produces = "application/json")
public class RegistrationEndpoint {

    @Autowired
    private GameUserRepository gameUserRepository;

    @Autowired
    private RegistrationValidatorService registrationValidatorService;

    @PostMapping("/registration")
    public String registration(@RequestBody RegistrationDTO registrationDTO) {
        if (!isValidRegistration(registrationDTO)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        GameUser gameUser = GameUser.builder()
                .userName(registrationDTO.getUsername())
                .password(registrationDTO.getPassword())
                .ladderRank(10000)
                .level(1)
                .wins(0)
                .loses(0)
                .rank(UserGameRank.GHOUL)
                .ratio(0).build();
        gameUserRepository.save(gameUser);
        log.info(" Beérkezett dummy adat: " + registrationDTO.toString());
        return "{\n" +
                "\t\"response\" : \"OK\"\n" +
                "}";
    }

    private boolean isValidRegistration(RegistrationDTO registrationDTO) {
        return registrationValidatorService.isUserNameValid(registrationDTO.getUsername())
                && registrationValidatorService.isPasswordValid(registrationDTO.getPassword())
                && registrationValidatorService.isValidEmail(registrationDTO.getEmail());
    }

}
