package anime.heroes.projekt.util.polling;

public interface PollingAction<T> {

    T run();

}
