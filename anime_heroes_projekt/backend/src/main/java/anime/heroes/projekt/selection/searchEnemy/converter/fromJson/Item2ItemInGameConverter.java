package anime.heroes.projekt.selection.searchEnemy.converter.fromJson;

import anime.heroes.projekt.selection.items.Item;
import anime.heroes.projekt.selection.searchEnemy.dto.ItemInGame;
import org.springframework.stereotype.Service;

@Service
public class Item2ItemInGameConverter {

    public ItemInGame convert(Item item){
        return ItemInGame.builder()
                .name(item.getName())
                .isActive(item.isIsActive())
                .imageName(item.getImg())
                .description("Leírás")/*TODO ezt még a leíró json-ben ki kell egészíteni*/
                .build();
    }

}
