package anime.heroes.projekt.registration.service;

import anime.heroes.projekt.repository.GameUserRepository;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RegistrationValidatorService {

    private static final String REGEX = "^(.+)@(.+)$";

    @Autowired
    private GameUserRepository gameUserRepository;

    public boolean isUserNameValid(String userName) {
        return this.gameUserRepository.findByUserName(userName) == null;
    }

    //TODO Ide még kell, hogy a kliens mindkét passwordöt felküldje, és azokat is össze hasonítsuk.
    public boolean isPasswordValid(String password) {
        return password.length() > 5 ;
    }

    public boolean isValidEmail(String email) {
        return EmailValidator.getInstance().isValid(email);
    }

}
