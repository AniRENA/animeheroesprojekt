package anime.heroes.projekt.game.effect.executors.shield;

import anime.heroes.projekt.changeTurn.dto.TurnChangeCharacter;
import anime.heroes.projekt.game.effect.EffectHandler;
import anime.heroes.projekt.game.effect.EffectHandlerBase;
import anime.heroes.projekt.selection.searchEnemy.dto.fromJson.Effect;
import anime.heroes.projekt.selection.searchEnemy.dto.fromJson.EffectType;

import java.util.List;

@EffectHandler(EffectType.SHIELD_DR)
public class ShieldDRHandler extends EffectHandlerBase {

    @Override
    public void calculateEffect(List<TurnChangeCharacter> charactersInGameState, List<TurnChangeCharacter> enemyCharactersInGameState, Effect effect, int target) {
        System.out.println("SHIELD_DR");
    }
}
