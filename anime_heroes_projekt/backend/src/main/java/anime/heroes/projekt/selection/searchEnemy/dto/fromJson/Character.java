package anime.heroes.projekt.selection.searchEnemy.dto.fromJson;

import lombok.Data;

@Data
public class Character {
    private String name;
    private Skill[] skills;
    private Skill[] altSkills;
    private String img;
    private int health;
    private int energy;
    private int fury;
}
