package anime.heroes.projekt.selection.searchEnemy;

import anime.heroes.projekt.SearchEnemyDBService;
import anime.heroes.projekt.authentication.AuthenticationProvider;
import anime.heroes.projekt.entity.SearchEnemy;
import anime.heroes.projekt.repository.SearchEnemyRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class SearchEnemyService {

    private static final int WAITING_MILLIS = 1000;

    private final AuthenticationProvider authenticationProvider;
    private final SearchEnemyRepository searchEnemyRepository;
    private final SearchEnemyDBService searchEnemyService;

    public SearchEnemyService(AuthenticationProvider authenticationProvider,
                              SearchEnemyRepository searchEnemyRepository,
                              SearchEnemyDBService searchEnemyService) {
        this.authenticationProvider = authenticationProvider;
        this.searchEnemyRepository = searchEnemyRepository;
        this.searchEnemyService = searchEnemyService;
    }

    /**
     * Működésée: 15 secig keres, ha nem talál akkor várja, hogy más megkeresse.
     * */
    Match searchLoop() {
        String userName = authenticationProvider.getAuthentication().getName();
        int i = 0;
        boolean inserted = false;
        while (true) {
            i++;
            if (i < 15) {
                SearchEnemy searchEnemy = searchEnemyService.search();
                if (searchEnemy != null) {
                    return Match.builder().player1(searchEnemy.getPlayerOne()).player2(searchEnemy.getPlayerTwo()).build();
                }
            } else {
                if (!inserted) {
                    SearchEnemy insert = SearchEnemy.builder().playerOne(userName).build();
                    searchEnemyService.saveInNewTransaction(insert);
                    inserted = true;
                }
                SearchEnemy maybe = searchEnemyService.checkHasPairWhileWaiting(userName);
                if (maybe != null) {
                    searchEnemyRepository.delete(maybe);
                    return Match.builder().player1(maybe.getPlayerOne()).player2(maybe.getPlayerTwo()).build();
                }
            }
            try {
                Thread.sleep(WAITING_MILLIS);
            } catch (InterruptedException e) {
                log.error("Search loop waiting exception: " + e);
            }
        }
    }

}
