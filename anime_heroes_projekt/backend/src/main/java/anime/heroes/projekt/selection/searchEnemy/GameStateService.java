package anime.heroes.projekt.selection.searchEnemy;

import anime.heroes.projekt.changeTurn.dto.ChangeTurnSkill;
import anime.heroes.projekt.changeTurn.dto.TurnChangeCharacter;
import anime.heroes.projekt.common.CharacterName;
import anime.heroes.projekt.entity.GameState;
import anime.heroes.projekt.entity.GameStateId;
import anime.heroes.projekt.entity.json.gameState.State;
import anime.heroes.projekt.file.ResourceLoader;
import anime.heroes.projekt.game.cost.CostService;
import anime.heroes.projekt.repository.GameStateRepository;
import anime.heroes.projekt.selection.searchEnemy.dto.CharacterInGame;
import anime.heroes.projekt.selection.searchEnemy.dto.SearchEnemy;
import anime.heroes.projekt.selection.searchEnemy.dto.fromJson.Character;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Service
public class GameStateService {

    private final GameStateRepository gameStateRepository;
    private final CostService costService;

    public GameStateService(GameStateRepository gameStateRepository, CostService costService) {
        this.gameStateRepository = gameStateRepository;
        this.costService = costService;
    }

    GameState initStateInDatabase(String playerOne, String playerTwo, String gameType, SearchEnemy searchEnemy){

        ResourceLoader resourceLoader = new ResourceLoader();

        List<Character> charactersPlayerOne = new ArrayList<>();
        List<Character> charactersPlayerTwo = new ArrayList<>();

        searchEnemy.getCharacters().stream().map(CharacterInGame::getName).forEach(name -> {
            try {
                charactersPlayerOne.add(new Gson().fromJson(resourceLoader.readCharacterAsJSON(CharacterName.valueOf(name.toUpperCase())), Character.class));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        searchEnemy.getEnemyCharacters().stream().map(CharacterInGame::getName).forEach(name ->{
            try {
                charactersPlayerTwo.add(new Gson().fromJson(resourceLoader.readCharacterAsJSON(CharacterName.valueOf(name.toUpperCase())), Character.class));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        State state = new State();
        state.setPlayerOneName(playerOne);
        state.setPlayerTwoName(playerTwo);
        state.setPlayerOneCharacters(searchEnemy.getCharacters().stream().map(character -> convertTo(character, charactersPlayerOne.stream()
                .filter(c -> c.getName().equals(character.getName()))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Invalid character"))))
                .collect(Collectors.toList()));
        state.setPlayerTwoCharacters(searchEnemy.getEnemyCharacters().stream().map(character -> convertTo(character, charactersPlayerTwo.stream()
                .filter(c -> c.getName().equals(character.getName()))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Invalid character"))))
                .collect(Collectors.toList()));

        state.setPlayerOneCosts(costService.generateFirstStarterPlayerCosts());
        state.setPlayerTwoCosts(costService.initCostsWithNullValues());

        return gameStateRepository.save(GameState.builder()
                .gameStateId(GameStateId.builder()
                        .playerOne(playerOne)
                        .playerTwo(playerTwo).build())
                .gameType(gameType)
                .state(new Gson().toJson(state)).build());
    }

    private TurnChangeCharacter convertTo(CharacterInGame characterInGame, Character character){
        Stream.of(character.getSkills()).forEach(skill -> {
            skill.setActualCd(skill.getActualCd());
            skill.setActualCost(skill.getActualCost());
        });
        return TurnChangeCharacter.builder()
                .name(characterInGame.getName())
                .imageName(characterInGame.getImageName())
                .health(characterInGame.getHealth())
                .actualHealth(characterInGame.getActualHealth())
                .item(characterInGame.getItem())
                .buffs(new ArrayList<>())
                .skills(Stream.of(character.getSkills()).collect(Collectors.toList()))
                .altSkills(Stream.of(character.getAltSkills()).collect(Collectors.toList())).build();
    }

}
