package anime.heroes.projekt.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "game_user")
public class GameUser {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;

    @Column(name = "username", nullable = false)
    private String userName;

    @Column(name = "password", nullable = false)
    private String password;

    @Enumerated(EnumType.STRING)
    private UserGameRank rank;

    @Column(name = "clan")
    private String clan;

    @Column(name = "lvl", nullable = false)
    private int level;

    @Column(name = "ladder_rank")
    private Integer ladderRank;

    @Column(name = "wins", nullable = false)
    private int wins;

    @Column(name = "loses", nullable = false)
    private int loses;

    @Column(name = "ratio", nullable = false)
    private int ratio;

    @ManyToMany
    @JoinTable(
            name = "game_user_game_character",
            joinColumns = @JoinColumn(name = "game_user_id"),
            inverseJoinColumns = @JoinColumn(name = "game_character_id"))
    private List<GameCharacter> characters;

    @ManyToMany
    @JoinTable(
            name = "game_user_game_item",
            joinColumns = @JoinColumn(name = "game_user_id"),
            inverseJoinColumns = @JoinColumn(name = "game_item_id"))
    private List<GameItem> items;

    @Column(name="selected_characters")
    @Lob
    private String selectedCharacters;
}
