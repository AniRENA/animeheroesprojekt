package anime.heroes.projekt.game.effect.executors.heal;

import anime.heroes.projekt.changeTurn.dto.TurnChangeCharacter;
import anime.heroes.projekt.game.effect.EffectHandler;
import anime.heroes.projekt.game.effect.EffectHandlerBase;
import anime.heroes.projekt.selection.searchEnemy.dto.fromJson.Effect;
import anime.heroes.projekt.selection.searchEnemy.dto.fromJson.EffectType;

import java.util.List;

@EffectHandler(EffectType.HEAL_MORE_ROUND)
public class HealMoreRoundHandler extends EffectHandlerBase {

    @Override
    public void calculateEffect(List<TurnChangeCharacter> charactersInGameState, List<TurnChangeCharacter> enemyCharactersInGameState, Effect effect, int target) {
        System.out.println("HEAL_MORE_ROUND");
    }
}
