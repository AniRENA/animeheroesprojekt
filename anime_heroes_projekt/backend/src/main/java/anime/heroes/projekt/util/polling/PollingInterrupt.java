package anime.heroes.projekt.util.polling;

public interface PollingInterrupt {

    boolean isInterrupted();

}
