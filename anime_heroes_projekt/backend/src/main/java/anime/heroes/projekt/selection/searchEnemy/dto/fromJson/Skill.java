package anime.heroes.projekt.selection.searchEnemy.dto.fromJson;

import lombok.Data;

import java.util.List;

@Data
public class Skill {
    private int cd;
    private Effect[] effects;
    private String img;
    private Cost[] cost;
    private int actualCd;
    private Cost[] actualCost;
    private String name;
    private String description;
    private List<anime.heroes.projekt.selection.searchEnemy.dto.Class> classes;
    private TargetsOfSkill targetsOfSkill;
}
