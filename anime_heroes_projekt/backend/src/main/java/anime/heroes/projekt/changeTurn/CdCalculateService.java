package anime.heroes.projekt.changeTurn;

import anime.heroes.projekt.changeTurn.dto.ChangeTurnSkill;
import anime.heroes.projekt.changeTurn.dto.TurnChangeCharacter;
import anime.heroes.projekt.selection.searchEnemy.dto.fromJson.Skill;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
class CdCalculateService {

    void actualizeCd(List<TurnChangeCharacter> charactersInGameState, Skill usedSkill){
        charactersInGameState.forEach(character -> {
            character.getSkills().forEach(skill -> {
                if(skill.getName().equals(usedSkill.getName())){
                    skill.setActualCd(skill.getCd());
                }
            });
        });
    }

    void decreaseCd(List<TurnChangeCharacter> charactersInGameState, List<ChangeTurnSkill> usedSkills){
        List<String> usedSkillsName = usedSkills.stream().map(ChangeTurnSkill::getName).collect(Collectors.toList());
        charactersInGameState.forEach(character -> {
            character.getSkills().forEach(skill -> {
                if(!usedSkillsName.contains(skill.getName()) && skill.getActualCd() > 0){
                    skill.setActualCd(skill.getActualCd() - 1);
                }
            });
        });
    }
}
