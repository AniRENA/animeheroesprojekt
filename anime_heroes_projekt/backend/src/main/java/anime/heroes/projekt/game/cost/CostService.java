package anime.heroes.projekt.game.cost;

import anime.heroes.projekt.selection.searchEnemy.dto.Cost;
import anime.heroes.projekt.selection.searchEnemy.dto.CostWithValue;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

@Slf4j
@Service
public class CostService {

    private final RandomIntegerGeneratorService randomIntegerGeneratorService;

    public CostService(RandomIntegerGeneratorService randomIntegerGeneratorService) {
        this.randomIntegerGeneratorService = randomIntegerGeneratorService;
    }

    public Cost generate() {
        int randomValue = randomIntegerGeneratorService.getRandomNumberInRange(0, 3);

        if (randomValue == Cost.ability.getOrderValue()) {
            return Cost.ability;
        } else if (randomValue == Cost.power.getOrderValue()) {
            return Cost.power;
        } else if (randomValue == Cost.tactic.getOrderValue()) {
            return Cost.tactic;
        } else if (randomValue == Cost.tool.getOrderValue()) {
            return Cost.tool;
        } else {
            throw new RuntimeException("Not supported randomly generated cost!");
        }
    }

    public List<CostWithValue> generateFirstStarterPlayerCosts() {
        List<CostWithValue> result = initCostsWithNullValues();

        Cost generatedCost = generate();

        result.forEach(costWithValue -> {
            if(costWithValue.getName() == generatedCost){
                costWithValue.setValue(1);
            }
        });

        return result;
    }

    public List<CostWithValue> generateSecondStartPlayerCosts() {
        List<CostWithValue> result = initCostsWithNullValues();
        IntStream.range(0, 3).forEach(index -> {
            Cost generatedCost = generate();

            result.forEach(costWithValue -> {
                if(costWithValue.getName() == generatedCost){
                    costWithValue.setValue(costWithValue.getValue() + 1);
                }
            });
        });

        return result;
    }

    public void generateRandomCostForCostState(List<CostWithValue> costState, int countOfGeneratedCosts) {
        IntStream.range(0, countOfGeneratedCosts).forEach(index -> {
            Cost generatedCost = generate();

            costState.forEach(costWithValue -> {
                if(costWithValue.getName() == generatedCost){
                    costWithValue.setValue(costWithValue.getValue() + 1);
                }
            });
        });
    }


    public List<CostWithValue> initCostsWithNullValues() {
        List<CostWithValue> result = new ArrayList<>();
        result.add(CostWithValue.builder().name(Cost.ability).value(0).build());
        result.add(CostWithValue.builder().name(Cost.power).value(0).build());
        result.add(CostWithValue.builder().name(Cost.tactic).value(0).build());
        result.add(CostWithValue.builder().name(Cost.tool).value(0).build());

        return result;
    }

}
