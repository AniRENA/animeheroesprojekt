package anime.heroes.projekt.selection.searchEnemy;

import anime.heroes.projekt.authentication.AuthenticationProvider;
import anime.heroes.projekt.entity.GameState;
import anime.heroes.projekt.entity.GameUser;
import anime.heroes.projekt.entity.json.gameState.State;
import anime.heroes.projekt.game.cost.CostService;
import anime.heroes.projekt.repository.GameStateRepository;
import anime.heroes.projekt.repository.GameUserRepository;
import anime.heroes.projekt.selection.dto.SelectionPayload;
import anime.heroes.projekt.selection.searchEnemy.converter.fromJson.Character2CharacterInGameConverter;
import anime.heroes.projekt.selection.searchEnemy.dto.*;
import anime.heroes.projekt.util.polling.Polling;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping(path = "/api", produces = "application/json")
public class SearchEnemyEndpoint {

    private static final int MAX_POLLER_SEC = 15;

    private final Character2CharacterInGameConverter character2CharacterInGameConverter;
    private final AuthenticationProvider authenticationProvider;
    private final GameUserRepository gameUserRepository;
    private final UserService userService;
    private final SelectionPayloadTransformToCharacterInGameService selectionPaylodTransformToCharacterInGameService;
    private final SearchEnemyService searchEnemyService;
    private final GameStateService gameStateService;
    private final CostService costService;
    private final GameStateRepository gameStateRepository;

    public SearchEnemyEndpoint(Character2CharacterInGameConverter character2CharacterInGameConverter,
                               AuthenticationProvider authenticationProvider,
                               GameUserRepository gameUserRepository,
                               UserService userService,
                               SelectionPayloadTransformToCharacterInGameService selectionPaylodTransformToCharacterInGameService,
                               SearchEnemyService searchEnemyService,
                               GameStateService gameStateService,
                               CostService costService,
                               GameStateRepository gameStateRepository
    ) {
        this.character2CharacterInGameConverter = character2CharacterInGameConverter;
        this.authenticationProvider = authenticationProvider;
        this.gameUserRepository = gameUserRepository;
        this.userService = userService;
        this.selectionPaylodTransformToCharacterInGameService = selectionPaylodTransformToCharacterInGameService;
        this.searchEnemyService = searchEnemyService;
        this.gameStateService = gameStateService;
        this.costService = costService;
        this.gameStateRepository = gameStateRepository;
    }

    /**
     * Ez a szolgáltatás már json fileból adja vissza a játékban lévő karaktereket. Egyenlőre a whoCanBeATarget érték be van
     * égetve, azt valami konstansokból feldolgozva kellen előállítania a kódnak.
     */
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PostMapping("/searchEnemy")
    public SearchEnemy searchEnemy(@RequestBody SelectionPayload selectionPayload) {
        String username = authenticationProvider.getAuthentication().getName();

        //TODO Ez a quick game-re van állítva, ha semmit nem választ ki a user, gyorsan lehet tesztelni.
        //át kell írni majd normálisra
        if(selectionPayload.getCharacters().contains(null)){
            return createGameWithRobot(username, selectionPayload);
        }

        setSelectedCharactersToUserInDB(selectionPayload, username);

        List<CharacterInGame> userCharacters = selectionPaylodTransformToCharacterInGameService.createCharacterInGames(
                selectionPayload);

        SearchEnemy searchEnemy = new SearchEnemy();
        Match match = searchEnemyService.searchLoop();
        String enemyName;
        if(match.getPlayer1().equals(username)){
            enemyName = match.getPlayer2();
            searchEnemy.setStartsTheGame(true);
        }else {
            enemyName = match.getPlayer1();
            searchEnemy.setStartsTheGame(false);
        }

        List<CharacterInGame> enemyCharacters = selectionPaylodTransformToCharacterInGameService.createCharacterInGames(
                createSelectionPaylodFromEnemyDBState(enemyName));

        searchEnemy.setFind(true);
        searchEnemy.setUser(userService.createUserInfoFromUserName(username));
        searchEnemy.setEnemyUser(userService.createUserInfoFromUserName(enemyName));
        searchEnemy.setCharacters(userCharacters);
        searchEnemy.setEnemyCharacters(enemyCharacters);

        generateIdForAllSkills(searchEnemy);

        setActualCdToZero(userCharacters);
        setActualCdToZero(enemyCharacters);

        /*Ha talált párt, akkor aki a játékot kezdi, az állítja be db-ben a gameState-t, hogy csak egyszer
        * legyen az beállítva!*/
        if(searchEnemy.isStartsTheGame()){
            GameState gameState = gameStateService.initStateInDatabase(searchEnemy.getUser().getUserName(),
                    searchEnemy.getEnemyUser().getUserName(), "LADDER", searchEnemy);

            anime.heroes.projekt.entity.json.gameState.State state = new Gson().fromJson(gameState.getState(), State.class);

            searchEnemy.setCosts(state.getPlayerOneCosts());
        } else {
            GameState gameState = new Polling<>(MAX_POLLER_SEC, () -> gameStateRepository.getByPlayerOneAndPlayerTwo(enemyName)).poll();
            anime.heroes.projekt.entity.json.gameState.State state = new Gson().fromJson(gameState.getState(), State.class);
            searchEnemy.setCosts(state.getPlayerTwoCosts());
        }

        return searchEnemy;
    }

    private void generateIdForAllSkills(SearchEnemy searchEnemy){
        int id = 1;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < searchEnemy.getCharacters().get(i).getSkills().size(); j++) {
                searchEnemy.getCharacters().get(i).getSkills().get(j).setId(id);
                id++;
            }
        }
    }

    private SelectionPayload createSelectionPaylodFromEnemyDBState(String userName){
        String enemySelections = gameUserRepository.findByUserName(userName).getSelectedCharacters();
        return new Gson().fromJson(enemySelections, SelectionPayload.class);
    }

    private void setSelectedCharactersToUserInDB(SelectionPayload selectionPayload, String username) {
        Gson gson = new Gson();
        GameUser gameUser = gameUserRepository.findByUserName(username);
        gameUser.setSelectedCharacters(gson.toJson(selectionPayload));
        gameUserRepository.save(gameUser);
    }

    private void setActualCdToZero(List<CharacterInGame> characters){
        characters.forEach(character -> character.getSkills().forEach(skill -> skill.setActualCooldown(0)));
    }

    private SearchEnemy createGameWithRobot(String username, SelectionPayload selectionPayload) {

        return new Gson().fromJson("{\"find\":true,\"startsTheGame\":false,\"enemyUser\":{\"userName\":\"fremen\",\"imageName\":\"../../../assets/game/Hiyori.png\",\"rank\":\"STRAW_HAT_PIRATE\",\"clan\":null,\"level\":1,\"ladderRank\":10000,\"wins\":0,\"loses\":0,\"ratio\":0},\"enemyCharacters\":[{\"name\":\"Aang\",\"imageName\":\"../../../assets/characterFP/AangFP.png\",\"health\":100,\"actualHealth\":100,\"skills\":[{\"id\":0,\"name\":\"GoldenEyeItem\",\"description\":\"DoflamingoBulletYarn desc\",\"imageName\":\"../../../assets/characterItems/GoldenEyeItem.png\",\"classes\":[\"TACTICAL\",\"STRATEGY\"],\"costs\":[\"tool\",\"tool\",\"tool\"],\"state\":\"NOTHING\",\"whoCanBeATarget\":[0,1,2,3,4,5],\"cooldown\":2},{\"id\":0,\"name\":\"Zoroa\",\"description\":\"LeĂ\u00ADrĂˇsosososos\",\"imageName\":\"../../../assets/game/Zoro.png\",\"classes\":[\"TACTICAL\",\"STRATEGY\"],\"costs\":[\"tool\",\"tool\",\"tool\"],\"state\":\"NOTHING\",\"whoCanBeATarget\":[0,1,2,3,4,5],\"cooldown\":2},{\"id\":0,\"name\":\"Zorob\",\"description\":\"LeĂ\u00ADrĂˇsosososos\",\"imageName\":\"../../../assets/game/Zoro.png\",\"classes\":[\"TACTICAL\",\"STRATEGY\"],\"costs\":[\"tool\",\"tool\",\"tool\"],\"state\":\"NOTHING\",\"whoCanBeATarget\":[0,1,2,3,4,5],\"cooldown\":2},{\"id\":0,\"name\":\"Zoroc\",\"description\":\"LeĂ\u00ADrĂˇsosososos\",\"imageName\":\"../../../assets/game/Zoro.png\",\"classes\":[\"TACTICAL\",\"STRATEGY\"],\"costs\":[\"tool\",\"tool\",\"tool\"],\"state\":\"NOTHING\",\"whoCanBeATarget\":[0,1,2,3,4,5],\"cooldown\":2},{\"id\":0,\"name\":\"Zorod\",\"description\":\"LeĂ\u00ADrĂˇsosososos\",\"imageName\":\"../../../assets/game/Zoro.png\",\"classes\":[\"TACTICAL\",\"STRATEGY\"],\"costs\":[\"tool\",\"tool\",\"tool\"],\"state\":\"NOTHING\",\"whoCanBeATarget\":[0,1,2,3,4,5],\"cooldown\":2},{\"id\":0,\"name\":\"Zoroe\",\"description\":\"LeĂ\u00ADrĂˇsosososos\",\"imageName\":\"../../../assets/game/Zoro.png\",\"classes\":[\"TACTICAL\",\"STRATEGY\"],\"costs\":[\"tool\",\"tool\",\"tool\"],\"state\":\"NOTHING\",\"whoCanBeATarget\":[0,1,2,3,4,5],\"cooldown\":2},{\"id\":0,\"name\":\"Zorof\",\"description\":\"LeĂ\u00ADrĂˇsosososos\",\"imageName\":\"../../../assets/game/Zoro.png\",\"classes\":[\"TACTICAL\",\"STRATEGY\"],\"costs\":[\"tool\",\"tool\",\"tool\"],\"state\":\"NOTHING\",\"whoCanBeATarget\":[0,1,2,3,4,5],\"cooldown\":2}],\"item\":{\"name\":\"DemonSwordItem\",\"imageName\":\"../../../assets/characterItems/DemonSwordItem.png\",\"description\":\"Leírás\",\"active\":false}},{\"name\":\"Akeno\",\"imageName\":\"../../../assets/characterFP/AkenoFP.png\",\"health\":100,\"actualHealth\":100,\"skills\":[{\"id\":0,\"name\":\"GoldenEyeItem\",\"description\":\"DoflamingoBulletYarn desc\",\"imageName\":\"../../../assets/characterItems/GoldenEyeItem.png\",\"classes\":[\"TACTICAL\",\"STRATEGY\"],\"costs\":[\"tool\",\"tool\",\"tool\"],\"state\":\"NOTHING\",\"whoCanBeATarget\":[0,1,2,3,4,5],\"cooldown\":2},{\"id\":0,\"name\":\"Zoroa\",\"description\":\"LeĂ\u00ADrĂˇsosososos\",\"imageName\":\"../../../assets/game/Zoro.png\",\"classes\":[\"TACTICAL\",\"STRATEGY\"],\"costs\":[\"tool\",\"tool\",\"tool\"],\"state\":\"NOTHING\",\"whoCanBeATarget\":[0,1,2,3,4,5],\"cooldown\":2},{\"id\":0,\"name\":\"Zorob\",\"description\":\"LeĂ\u00ADrĂˇsosososos\",\"imageName\":\"../../../assets/game/Zoro.png\",\"classes\":[\"TACTICAL\",\"STRATEGY\"],\"costs\":[\"tool\",\"tool\",\"tool\"],\"state\":\"NOTHING\",\"whoCanBeATarget\":[0,1,2,3,4,5],\"cooldown\":2},{\"id\":0,\"name\":\"Zoroc\",\"description\":\"LeĂ\u00ADrĂˇsosososos\",\"imageName\":\"../../../assets/game/Zoro.png\",\"classes\":[\"TACTICAL\",\"STRATEGY\"],\"costs\":[\"tool\",\"tool\",\"tool\"],\"state\":\"NOTHING\",\"whoCanBeATarget\":[0,1,2,3,4,5],\"cooldown\":2},{\"id\":0,\"name\":\"Zorod\",\"description\":\"LeĂ\u00ADrĂˇsosososos\",\"imageName\":\"../../../assets/game/Zoro.png\",\"classes\":[\"TACTICAL\",\"STRATEGY\"],\"costs\":[\"tool\",\"tool\",\"tool\"],\"state\":\"NOTHING\",\"whoCanBeATarget\":[0,1,2,3,4,5],\"cooldown\":2},{\"id\":0,\"name\":\"Zoroe\",\"description\":\"LeĂ\u00ADrĂˇsosososos\",\"imageName\":\"../../../assets/game/Zoro.png\",\"classes\":[\"TACTICAL\",\"STRATEGY\"],\"costs\":[\"tool\",\"tool\",\"tool\"],\"state\":\"NOTHING\",\"whoCanBeATarget\":[0,1,2,3,4,5],\"cooldown\":2},{\"id\":0,\"name\":\"Zorof\",\"description\":\"LeĂ\u00ADrĂˇsosososos\",\"imageName\":\"../../../assets/game/Zoro.png\",\"classes\":[\"TACTICAL\",\"STRATEGY\"],\"costs\":[\"tool\",\"tool\",\"tool\"],\"state\":\"NOTHING\",\"whoCanBeATarget\":[0,1,2,3,4,5],\"cooldown\":2}],\"item\":{\"name\":\"DemonSwordItem\",\"imageName\":\"../../../assets/characterItems/DemonSwordItem.png\",\"description\":\"Leírás\",\"active\":false}},{\"name\":\"Allen\",\"imageName\":\"../../../assets/characterFP/AllenFP.png\",\"health\":100,\"actualHealth\":100,\"skills\":[{\"id\":0,\"name\":\"GoldenEyeItem\",\"description\":\"DoflamingoBulletYarn desc\",\"imageName\":\"../../../assets/characterItems/GoldenEyeItem.png\",\"classes\":[\"TACTICAL\",\"STRATEGY\"],\"costs\":[\"tool\",\"tool\",\"tool\"],\"state\":\"NOTHING\",\"whoCanBeATarget\":[0,1,2,3,4,5],\"cooldown\":2},{\"id\":0,\"name\":\"Zoroa\",\"description\":\"LeĂ\u00ADrĂˇsosososos\",\"imageName\":\"../../../assets/game/Zoro.png\",\"classes\":[\"TACTICAL\",\"STRATEGY\"],\"costs\":[\"tool\",\"tool\",\"tool\"],\"state\":\"NOTHING\",\"whoCanBeATarget\":[0,1,2,3,4,5],\"cooldown\":2},{\"id\":0,\"name\":\"Zorob\",\"description\":\"LeĂ\u00ADrĂˇsosososos\",\"imageName\":\"../../../assets/game/Zoro.png\",\"classes\":[\"TACTICAL\",\"STRATEGY\"],\"costs\":[\"tool\",\"tool\",\"tool\"],\"state\":\"NOTHING\",\"whoCanBeATarget\":[0,1,2,3,4,5],\"cooldown\":2},{\"id\":0,\"name\":\"Zoroc\",\"description\":\"LeĂ\u00ADrĂˇsosososos\",\"imageName\":\"../../../assets/game/Zoro.png\",\"classes\":[\"TACTICAL\",\"STRATEGY\"],\"costs\":[\"tool\",\"tool\",\"tool\"],\"state\":\"NOTHING\",\"whoCanBeATarget\":[0,1,2,3,4,5],\"cooldown\":2},{\"id\":0,\"name\":\"Zorod\",\"description\":\"LeĂ\u00ADrĂˇsosososos\",\"imageName\":\"../../../assets/game/Zoro.png\",\"classes\":[\"TACTICAL\",\"STRATEGY\"],\"costs\":[\"tool\",\"tool\",\"tool\"],\"state\":\"NOTHING\",\"whoCanBeATarget\":[0,1,2,3,4,5],\"cooldown\":2},{\"id\":0,\"name\":\"Zoroe\",\"description\":\"LeĂ\u00ADrĂˇsosososos\",\"imageName\":\"../../../assets/game/Zoro.png\",\"classes\":[\"TACTICAL\",\"STRATEGY\"],\"costs\":[\"tool\",\"tool\",\"tool\"],\"state\":\"NOTHING\",\"whoCanBeATarget\":[0,1,2,3,4,5],\"cooldown\":2},{\"id\":0,\"name\":\"Zorof\",\"description\":\"LeĂ\u00ADrĂˇsosososos\",\"imageName\":\"../../../assets/game/Zoro.png\",\"classes\":[\"TACTICAL\",\"STRATEGY\"],\"costs\":[\"tool\",\"tool\",\"tool\"],\"state\":\"NOTHING\",\"whoCanBeATarget\":[0,1,2,3,4,5],\"cooldown\":2}],\"item\":{\"name\":\"DemonSwordItem\",\"imageName\":\"../../../assets/characterItems/DemonSwordItem.png\",\"description\":\"Leírás\",\"active\":false}}],\"user\":{\"userName\":\"senyor\",\"imageName\":\"../../../assets/game/Hiyori.png\",\"rank\":\"AIRBENDER\",\"clan\":\"asd noobs\",\"level\":2,\"ladderRank\":3000,\"wins\":0,\"loses\":20,\"ratio\":-20},\"characters\":[{\"name\":\"Inuyasha\",\"imageName\":\"../../../assets/characterFP/InuyashaFP.png\",\"health\":100,\"actualHealth\":100,\"skills\":[{\"id\":1,\"name\":\"GoldenEyeItem\",\"description\":\"DoflamingoBulletYarn desc\",\"imageName\":\"../../../assets/characterItems/GoldenEyeItem.png\",\"classes\":[\"TACTICAL\",\"STRATEGY\"],\"costs\":[\"tool\",\"tool\",\"tool\"],\"state\":\"NOTHING\",\"whoCanBeATarget\":[0,1,2,3,4,5],\"cooldown\":2},{\"id\":2,\"name\":\"Zoroa\",\"description\":\"LeĂ\u00ADrĂˇsosososos\",\"imageName\":\"../../../assets/game/Zoro.png\",\"classes\":[\"TACTICAL\",\"STRATEGY\"],\"costs\":[\"tool\",\"tool\",\"tool\"],\"state\":\"NOTHING\",\"whoCanBeATarget\":[0,1,2,3,4,5],\"cooldown\":2},{\"id\":3,\"name\":\"Zorob\",\"description\":\"LeĂ\u00ADrĂˇsosososos\",\"imageName\":\"../../../assets/game/Zoro.png\",\"classes\":[\"TACTICAL\",\"STRATEGY\"],\"costs\":[\"tool\",\"tool\",\"tool\"],\"state\":\"NOTHING\",\"whoCanBeATarget\":[0,1,2,3,4,5],\"cooldown\":2},{\"id\":4,\"name\":\"Zoroc\",\"description\":\"LeĂ\u00ADrĂˇsosososos\",\"imageName\":\"../../../assets/game/Zoro.png\",\"classes\":[\"TACTICAL\",\"STRATEGY\"],\"costs\":[\"tool\",\"tool\",\"tool\"],\"state\":\"NOTHING\",\"whoCanBeATarget\":[0,1,2,3,4,5],\"cooldown\":2},{\"id\":5,\"name\":\"Zorod\",\"description\":\"LeĂ\u00ADrĂˇsosososos\",\"imageName\":\"../../../assets/game/Zoro.png\",\"classes\":[\"TACTICAL\",\"STRATEGY\"],\"costs\":[\"tool\",\"tool\",\"tool\"],\"state\":\"NOTHING\",\"whoCanBeATarget\":[0,1,2,3,4,5],\"cooldown\":2},{\"id\":6,\"name\":\"Zoroe\",\"description\":\"LeĂ\u00ADrĂˇsosososos\",\"imageName\":\"../../../assets/game/Zoro.png\",\"classes\":[\"TACTICAL\",\"STRATEGY\"],\"costs\":[\"tool\",\"tool\",\"tool\"],\"state\":\"NOTHING\",\"whoCanBeATarget\":[0,1,2,3,4,5],\"cooldown\":2},{\"id\":7,\"name\":\"Zorof\",\"description\":\"LeĂ\u00ADrĂˇsosososos\",\"imageName\":\"../../../assets/game/Zoro.png\",\"classes\":[\"TACTICAL\",\"STRATEGY\"],\"costs\":[\"tool\",\"tool\",\"tool\"],\"state\":\"NOTHING\",\"whoCanBeATarget\":[0,1,2,3,4,5],\"cooldown\":2}],\"item\":{\"name\":\"AmonsMaskItem\",\"imageName\":\"../../../assets/characterItems/AmonsMaskItem.png\",\"description\":\"Leírás\",\"active\":false}},{\"name\":\"Jellal\",\"imageName\":\"../../../assets/characterFP/JellalFP.png\",\"health\":100,\"actualHealth\":100,\"skills\":[{\"id\":8,\"name\":\"GoldenEyeItem\",\"description\":\"DoflamingoBulletYarn desc\",\"imageName\":\"../../../assets/characterItems/GoldenEyeItem.png\",\"classes\":[\"TACTICAL\",\"STRATEGY\"],\"costs\":[\"tool\",\"tool\",\"tool\"],\"state\":\"NOTHING\",\"whoCanBeATarget\":[0,1,2,3,4,5],\"cooldown\":2},{\"id\":9,\"name\":\"Zoroa\",\"description\":\"LeĂ\u00ADrĂˇsosososos\",\"imageName\":\"../../../assets/game/Zoro.png\",\"classes\":[\"TACTICAL\",\"STRATEGY\"],\"costs\":[\"tool\",\"tool\",\"tool\"],\"state\":\"NOTHING\",\"whoCanBeATarget\":[0,1,2,3,4,5],\"cooldown\":2},{\"id\":10,\"name\":\"Zorob\",\"description\":\"LeĂ\u00ADrĂˇsosososos\",\"imageName\":\"../../../assets/game/Zoro.png\",\"classes\":[\"TACTICAL\",\"STRATEGY\"],\"costs\":[\"tool\",\"tool\",\"tool\"],\"state\":\"NOTHING\",\"whoCanBeATarget\":[0,1,2,3,4,5],\"cooldown\":2},{\"id\":11,\"name\":\"Zoroc\",\"description\":\"LeĂ\u00ADrĂˇsosososos\",\"imageName\":\"../../../assets/game/Zoro.png\",\"classes\":[\"TACTICAL\",\"STRATEGY\"],\"costs\":[\"tool\",\"tool\",\"tool\"],\"state\":\"NOTHING\",\"whoCanBeATarget\":[0,1,2,3,4,5],\"cooldown\":2},{\"id\":12,\"name\":\"Zorod\",\"description\":\"LeĂ\u00ADrĂˇsosososos\",\"imageName\":\"../../../assets/game/Zoro.png\",\"classes\":[\"TACTICAL\",\"STRATEGY\"],\"costs\":[\"tool\",\"tool\",\"tool\"],\"state\":\"NOTHING\",\"whoCanBeATarget\":[0,1,2,3,4,5],\"cooldown\":2},{\"id\":13,\"name\":\"Zoroe\",\"description\":\"LeĂ\u00ADrĂˇsosososos\",\"imageName\":\"../../../assets/game/Zoro.png\",\"classes\":[\"TACTICAL\",\"STRATEGY\"],\"costs\":[\"tool\",\"tool\",\"tool\"],\"state\":\"NOTHING\",\"whoCanBeATarget\":[0,1,2,3,4,5],\"cooldown\":2},{\"id\":14,\"name\":\"Zorof\",\"description\":\"LeĂ\u00ADrĂˇsosososos\",\"imageName\":\"../../../assets/game/Zoro.png\",\"classes\":[\"TACTICAL\",\"STRATEGY\"],\"costs\":[\"tool\",\"tool\",\"tool\"],\"state\":\"NOTHING\",\"whoCanBeATarget\":[0,1,2,3,4,5],\"cooldown\":2}],\"item\":{\"name\":\"AmonsMaskItem\",\"imageName\":\"../../../assets/characterItems/AmonsMaskItem.png\",\"description\":\"Leírás\",\"active\":false}},{\"name\":\"Genos\",\"imageName\":\"../../../assets/characterFP/GenosFP.png\",\"health\":100,\"actualHealth\":100,\"skills\":[{\"id\":15,\"name\":\"GoldenEyeItem\",\"description\":\"DoflamingoBulletYarn desc\",\"imageName\":\"../../../assets/characterItems/GoldenEyeItem.png\",\"classes\":[\"TACTICAL\",\"STRATEGY\"],\"costs\":[\"tool\",\"tool\",\"tool\"],\"state\":\"NOTHING\",\"whoCanBeATarget\":[0,1,2,3,4,5],\"cooldown\":2},{\"id\":16,\"name\":\"Zoroa\",\"description\":\"LeĂ\u00ADrĂˇsosososos\",\"imageName\":\"../../../assets/game/Zoro.png\",\"classes\":[\"TACTICAL\",\"STRATEGY\"],\"costs\":[\"tool\",\"tool\",\"tool\"],\"state\":\"NOTHING\",\"whoCanBeATarget\":[0,1,2,3,4,5],\"cooldown\":2},{\"id\":17,\"name\":\"Zorob\",\"description\":\"LeĂ\u00ADrĂˇsosososos\",\"imageName\":\"../../../assets/game/Zoro.png\",\"classes\":[\"TACTICAL\",\"STRATEGY\"],\"costs\":[\"tool\",\"tool\",\"tool\"],\"state\":\"NOTHING\",\"whoCanBeATarget\":[0,1,2,3,4,5],\"cooldown\":2},{\"id\":18,\"name\":\"Zoroc\",\"description\":\"LeĂ\u00ADrĂˇsosososos\",\"imageName\":\"../../../assets/game/Zoro.png\",\"classes\":[\"TACTICAL\",\"STRATEGY\"],\"costs\":[\"tool\",\"tool\",\"tool\"],\"state\":\"NOTHING\",\"whoCanBeATarget\":[0,1,2,3,4,5],\"cooldown\":2},{\"id\":19,\"name\":\"Zorod\",\"description\":\"LeĂ\u00ADrĂˇsosososos\",\"imageName\":\"../../../assets/game/Zoro.png\",\"classes\":[\"TACTICAL\",\"STRATEGY\"],\"costs\":[\"tool\",\"tool\",\"tool\"],\"state\":\"NOTHING\",\"whoCanBeATarget\":[0,1,2,3,4,5],\"cooldown\":2},{\"id\":20,\"name\":\"Zoroe\",\"description\":\"LeĂ\u00ADrĂˇsosososos\",\"imageName\":\"../../../assets/game/Zoro.png\",\"classes\":[\"TACTICAL\",\"STRATEGY\"],\"costs\":[\"tool\",\"tool\",\"tool\"],\"state\":\"NOTHING\",\"whoCanBeATarget\":[0,1,2,3,4,5],\"cooldown\":2},{\"id\":21,\"name\":\"Zorof\",\"description\":\"LeĂ\u00ADrĂˇsosososos\",\"imageName\":\"../../../assets/game/Zoro.png\",\"classes\":[\"TACTICAL\",\"STRATEGY\"],\"costs\":[\"tool\",\"tool\",\"tool\"],\"state\":\"NOTHING\",\"whoCanBeATarget\":[0,1,2,3,4,5],\"cooldown\":2}],\"item\":{\"name\":\"AmonsMaskItem\",\"imageName\":\"../../../assets/characterItems/AmonsMaskItem.png\",\"description\":\"Leírás\",\"active\":false}}],\"costs\":[{\"name\":\"ability\",\"value\":2},{\"name\":\"tactic\",\"value\":3},{\"name\":\"tool\",\"value\":4},{\"name\":\"power\",\"value\":5}]}", SearchEnemy.class);
    }

}
