package anime.heroes.projekt.selection.characters;

import anime.heroes.projekt.authentication.AuthenticationProvider;
import anime.heroes.projekt.common.CharacterName;
import anime.heroes.projekt.entity.GameCharacter;
import anime.heroes.projekt.file.ResourceLoader;
import anime.heroes.projekt.repository.GameUserRepository;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@RestController
@RequestMapping(path="/api", produces="application/json")
public class AllCharactersEndpoint {

    private final GameUserRepository gameUserRepository;
    private final AuthenticationProvider authenticationProvider;

    public AllCharactersEndpoint(GameUserRepository gameUserRepository, AuthenticationProvider authenticationProvider) {
        this.gameUserRepository = gameUserRepository;
        this.authenticationProvider = authenticationProvider;
    }

    @GetMapping("/allCharacters")
    public List<Character> getAllCharacters() {
        String authenticatedUser = authenticationProvider.getAuthentication().getName();
        List<CharacterName> ownedCharacters = gameUserRepository.findByUserName(authenticatedUser).getCharacters()
                .stream()
                .map(GameCharacter::getName)
                .collect(Collectors.toList());
        return Stream.of(CharacterName.values())
                .map(enumValue -> {
                    Character character = null;
                    try{
                        character = new Gson().fromJson(new ResourceLoader().readCharacterAsJSON(enumValue), Character.class);
                    }catch (IOException e){
                        log.error("Cannot parse character from {} JSON file!", enumValue);
                    }

                    return Character.builder()
                            .name(character.getName())
                            .img(character.getImg())
                            .owned(ownedCharacters.contains(enumValue)).build();
                })
                .collect(Collectors.toList());
    }
}
