package anime.heroes.projekt.selection.items;

import anime.heroes.projekt.authentication.AuthenticationProvider;
import anime.heroes.projekt.common.ItemName;
import anime.heroes.projekt.entity.GameItem;
import anime.heroes.projekt.file.ResourceLoader;
import anime.heroes.projekt.repository.GameUserRepository;
import anime.heroes.projekt.selection.characters.Character;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@RestController
@RequestMapping(path = "/api", produces = "application/json")
public class AllItemsEndpoint {

    private final GameUserRepository gameUserRepository;
    private final AuthenticationProvider authenticationProvider;

    public AllItemsEndpoint(GameUserRepository gameUserRepository, AuthenticationProvider authenticationProvider) {
        this.gameUserRepository = gameUserRepository;
        this.authenticationProvider = authenticationProvider;
    }

    @GetMapping("/allItems")
    public List<Item> getAllItems() {
        String authenticatedUser = authenticationProvider.getAuthentication().getName();
        List<ItemName> items = gameUserRepository.findByUserName(authenticatedUser).getItems().stream()
                .map(GameItem::getName)
                .collect(Collectors.toList());

        return Stream.of(ItemName.values()).map(itemName -> {
            Item item = null;
            try {
                item = new Gson().fromJson(new ResourceLoader().readItemAsJSON(itemName), Item.class);
                if (items.contains(itemName)) {
                    item.setOwned(true);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return item;
        }).collect(Collectors.toList());
    }

}
