package anime.heroes.projekt.selection.searchEnemy.dto.user;

import anime.heroes.projekt.entity.UserGameRank;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class UserInfo {
    private String userName;
    private String imageName;
    private UserGameRank rank;
    private String clan;
    private int level;
    private int ladderRank;
    private int wins;
    private int loses;
    private int ratio;
}
