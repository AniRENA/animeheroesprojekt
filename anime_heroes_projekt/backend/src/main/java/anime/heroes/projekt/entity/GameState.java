package anime.heroes.projekt.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "game_state")
public class GameState {

    @EmbeddedId
    private GameStateId gameStateId;

    //TODO Ez enum legyen majd ("Ladder", "Private", "tsb")
    @Column(name = "game_type", nullable = false)
    private String gameType;

    @Column(name = "state", nullable = false)
    private String state;

}
