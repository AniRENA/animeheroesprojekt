package anime.heroes.projekt.selection.characters;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Character {
    private String name;
    private String img;
    private boolean owned;
}
