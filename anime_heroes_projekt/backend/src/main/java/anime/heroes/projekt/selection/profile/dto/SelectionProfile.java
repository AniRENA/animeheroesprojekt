package anime.heroes.projekt.selection.profile.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SelectionProfile {
    private String userName;
    private String rank;
    private String clan;
    private int level;
    private int ladderRank;
    private int wins;
    private int loses;
    private int ratio;
}
