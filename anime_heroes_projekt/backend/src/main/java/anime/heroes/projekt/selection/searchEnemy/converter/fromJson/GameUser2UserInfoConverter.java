package anime.heroes.projekt.selection.searchEnemy.converter.fromJson;

import anime.heroes.projekt.entity.GameUser;
import anime.heroes.projekt.selection.searchEnemy.dto.user.UserInfo;
import org.springframework.stereotype.Service;

@Service
public class GameUser2UserInfoConverter {

    public UserInfo convert(GameUser gameUser){
        return UserInfo.builder()
                .userName(gameUser.getUserName())
                .imageName("../../../assets/game/Hiyori.png")/*TODO Ez is majd dbből jöjjön. */
                .rank(gameUser.getRank())
                .clan(gameUser.getClan())
                .level(gameUser.getLevel())
                .ladderRank(gameUser.getLadderRank())
                .wins(gameUser.getWins())
                .loses(gameUser.getLoses())
                .ratio(gameUser.getRatio())
                .build();
    }

}
