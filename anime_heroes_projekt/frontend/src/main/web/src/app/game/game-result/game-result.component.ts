import { Component, OnInit } from '@angular/core';
import { SoundService } from '../../services/sound.service';
import { GameMenuClickService } from '../services/game-menu-click.service';

@Component({
  selector: 'app-game-result',
  templateUrl: './game-result.component.html',
  styleUrls: ['./game-result.component.css']
})
export class GameResultComponent implements OnInit{

  public isWinner: boolean = true;

  constructor(
    private soundService: SoundService,
    private gameMenuClickService: GameMenuClickService
  ) { }

  ngOnInit(){
    this.gameMenuClickService.loseTheGame$.subscribe(value => {
      if(value){
        this.setLoser();
      }else{
        this.setWinner();
      }
    });
  }

  public getWindowbackGround(): string{
    if(!this.isWinner){
      return "../../../assets/game/loseWindow.png";
    }else{
      return "../../../assets/game/winWindow.png";
    }
  }

  public onClickHandler(): void{
    this.gameMenuClickService.sendExitTheGame();
  }

  private setLoser(): void{
    this.soundService.playLoseGameSong();
    this.isWinner = false;
  }

  private setWinner(): void{
    this.soundService.playGameWinSong();
    this.isWinner = true;
  }

}
