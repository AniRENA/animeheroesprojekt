import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-game-user-profiles',
  templateUrl: './game-user-profiles.component.html',
  styleUrls: ['./game-user-profiles.component.css']
})
export class GameUserProfilesComponent{

  @Input()
  public enemyUser: UserDetail;

  @Input()
  public user: UserDetail;

  constructor() { }

}
