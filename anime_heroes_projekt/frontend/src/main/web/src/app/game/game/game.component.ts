import { Component, Input } from '@angular/core';
import { DescriptionService } from '../services/description.service';
import { CostService } from '../services/cost.service';
import { delay } from 'rxjs/operators';
import { CharacterBuff } from '../dto/CharacterBuff';
import { SoundService } from '../../services/sound.service';
import { UsedSkillsService } from '../services/used-skills.service';
import { GameMenuClickService } from '../services/game-menu-click.service';
import { BuffOrderService } from '../services/buff-order.service';
import { TurnService } from '../services/turn.service';
import { HealthBarService } from '../services/health-bar.service';
import { TurnChangePrePhaseService } from '../services/turn-change-pre-phase.service';
import { RankService } from '../services/rank.service';
import { GameStateService } from '../services/game-state.service';
import { WebsocketService } from '../services/websocket.service';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent {

  @Input()
  public characters: Character[];

  @Input()
  public enemyCharacters: Character[];

  @Input()
  public enemyUserRank: string;

  @Input()
  public userRank: string;

  @Input()
  public ownTheRound: boolean;

  public userRankImage: string = "";

  public enemyRankImage: string = "";

  public userRankZindex: number = 1;

  public enemyRankZindex: number = 1;

  private enemyTargetAble: boolean[] = [false, false, false];

  private ownTargetAble: boolean[] = [false, false, false];

  public skillAvailabilities: boolean[][] = [
    [false, false, false, false, false, false, false],
    [false, false, false, false, false, false, false],
    [false, false, false, false, false, false, false]
  ];/**true érték esetben elszürkül, nem használható */

  private enemyCharacterBuffs: CharacterBuff[][] = [[], [], []];

  private friendlyCharacterBuffs: CharacterBuff[][] = [[], [], []];

  private healthBarDefaultWidth: number = 75;

  constructor(
    private descriptionService: DescriptionService,
    private costService: CostService,
    private soundService: SoundService,
    private usedSkillsService: UsedSkillsService,
    private gameMenuClickService: GameMenuClickService,
    private buffOrderService: BuffOrderService,
    private turnService: TurnService,
    private healthBarService: HealthBarService,
    private turnChangePrePhaseService: TurnChangePrePhaseService,
    private rankService: RankService,
    private gameStateService: GameStateService,
    private webSocketService: WebsocketService
  ) {}

   public handleEnemyBuffClick(indexOfEnemyCharacter: number, indexOfBuff: number): void{
    if(this.getWantToUseSkill() == null){
      let buff = this.enemyCharacterBuffs[indexOfEnemyCharacter][indexOfBuff]; 
      if(buff.undoAble == true) {
        this.enemyCharacterBuffs[indexOfEnemyCharacter].splice(indexOfBuff, 1);
        let position: number[] = this.getASkillPositionOnBoard(buff.skill);
        this.skillAvailabilities[position[0]][position[1]] = false;
        buff.skill.state = "NOTHING";
        this.costService.addChangeCost(buff.costs);
        this.usedSkillsService.removeFromUsedSkills(buff.skill);
      }
    }
   }

   public handleFriendlyBuffClick(indexOfFriendlyCharacter: number, indexOfBuff: number ): void{
    if(this.getWantToUseSkill() == null){
      let buff = this.friendlyCharacterBuffs[indexOfFriendlyCharacter][indexOfBuff]; 
      if(buff.undoAble == true) {
        this.friendlyCharacterBuffs[indexOfFriendlyCharacter].splice(indexOfBuff, 1);
        let position: number[] = this.getASkillPositionOnBoard(buff.skill);
        this.skillAvailabilities[position[0]][position[1]] = false;
        buff.skill.state = "NOTHING";
        this.costService.addChangeCost(buff.costs);
        this.usedSkillsService.removeFromUsedSkills(buff.skill);
      }
    }
   }

   public addEnemyCharacterBuff(enemyindex: number, characterBuff: CharacterBuff): void{
    this.enemyCharacterBuffs[enemyindex].push(characterBuff);
    this.soundService.playAttackGameSong();
   }

   public addFriendlyCharacterBuff(friendlyIndex: number, characterBuff: CharacterBuff): void{
     this.friendlyCharacterBuffs[friendlyIndex].push(characterBuff);
   }

  public handleSkillClick(skill: Skill): void {
    this.descriptionService.sendSelectedSkill(skill);
    let skillPosition = this.getASkillPositionOnBoard(skill);
    if(!this.skillAvailabilities[skillPosition[0]][skillPosition[1]]){
      this.setTargetAbleCharacters(skill);
    }
  }

  /**Ez állítja be a támadható karaktereket, amikor egy skillre kattintunk. */
  private setTargetAbleCharacters(skill: Skill): void {

    if(this.isEverySkillsNothingState() && skill.state === "NOTHING") {
      for(let i=0; i< this.ownTargetAble.length;i++) {
        if(skill.whoCanBeATarget.includes(i)) {
          this.ownTargetAble[i] = true;
        }
      }
      for(let i=3; i<this.enemyCharacters.length + 3; i++) {
        if(skill.whoCanBeATarget.includes(i)) {
          this.enemyTargetAble[i - 3] = true;
        }
      }
      this.setAllTheOtherSkillDisabled(skill);
      skill.state = 'WANT_TO_USE';
    } else if(skill.state === "WANT_TO_USE") {
      this.setSkillsNothingState();
      skill.state = 'NOTHING';
      this.checkAllSkillStateAndSet();
    }
  }

  private setSkillsNothingState(): void{
    for(let i=0; i< this.ownTargetAble.length;i++) {
        this.ownTargetAble[i] = false;
    }
    for(let i=0; i<this.enemyCharacters.length; i++) {
        this.enemyTargetAble[i] = false;
    }
  }

  /**Ez azt adja vissza, hogy nincs egy skill sem olyan állapotban, hogy ellenfélre rakásra vár.(want-to-use) 
   * állapotban egy sincs.
   */
  private isEverySkillsNothingState(): boolean{
    for(let i=0; i < this.ownTargetAble.length;i++) {
       if( this.ownTargetAble[i] == true){
         return false;
       }
    }
    for(let i=3; i<this.enemyCharacters.length + 3; i++) {
      if( this.ownTargetAble[i-3] == true){
        return false;
      }
    }
    return true;
  }

  public enemyImageClickHandler(enemy: Character, index: number): void{
    if(this.enemyTargetAble[index] == true) {
      let skill: Skill = this.getWantToUseSkill();
      if(skill != null){
        this.setSkillsNothingState();
        this.costService.changeCostsByDifference(this.costService.getCosts(skill));
        skill.state = "USED_IN_ROUND";
        skill.whoIsTheTarget = index + 3;
        this.costService.sendRequest();
        this.usedSkillsService.addNewlyUsedSkill(skill);
        let buff: CharacterBuff  = new CharacterBuff();
        buff.skill = skill;
        buff.costs = this.costService.getCosts(skill);
        buff.undoAble = true;
        this.addEnemyCharacterBuff(index, buff);
        this.checkAllSkillStateAndSet();
      }
    }
  }

  public friendlyImageClickHandler(character: Character, index: number): void{
    if(this.ownTargetAble[index] == true){
      let skill: Skill = this.getWantToUseSkill();
      if(skill != null){
        this.setSkillsNothingState();
        this.costService.changeCostsByDifference(this.costService.getCosts(skill));
        skill.state = "USED_IN_ROUND";
        skill.whoIsTheTarget = index;
        this.costService.sendRequest();
        this.usedSkillsService.addNewlyUsedSkill(skill);
        let buff: CharacterBuff  = new CharacterBuff();
        buff.skill = skill;
        buff.costs = this.costService.getCosts(skill);
        buff.undoAble = true;
        this.addFriendlyCharacterBuff(index, buff);
        this.checkAllSkillStateAndSet();
      }
    }
  }

  private getWantToUseSkill(): Skill{
    for(let i=0;i<this.characters.length;i++){
      for(let j=0;j<this.characters[i].skills.length;j++){
        if(this.characters[i].skills[j].state == "WANT_TO_USE"){
          return this.characters[i].skills[j];
        }
      }
    }
    return null;
  }

  private checkCooldowns(): void{
    for(let i=0;i<this.characters.length;i++){
      for(let j=0;j<this.characters[i].skills.length;j++){
        if(this.characters[i].skills[j].actualCooldown > 0){
          this.skillAvailabilities[i][j] = true;
        }
      }
    }
  }

  public isSkillAvailable(i: number, j: number): boolean{
    return this.skillAvailabilities[i][j];
  }

  ngAfterContentInit(){
    this.userRankZindex = this.rankService.getZindexOfRank(this.userRank);
    this.enemyRankZindex = this.rankService.getZindexOfRank(this.enemyUserRank);
    this.userRankImage = this.rankService.selectImageForRank(this.userRank);
    this.enemyRankImage = this.rankService.selectImageForRank(this.enemyUserRank);
    this.turnService.turnChangePrePhaseResponseArrive$.subscribe(value => {
      this.turnChangePrePhaseService.turnChangePrePhaseResponseHandler(
        value, 
        this.characters, 
        this.enemyCharacters, 
        this.friendlyCharacterBuffs, 
        this.enemyCharacterBuffs
        );
        this.webSocketService.send({"name": "almakorteszilva"});
    });
    this.webSocketService.connect((message: any) => {
      let turnChangeResponse: TurnChangeResponse = JSON.parse(message.body);
      let costsArrived: number[] = [0, 0, 0, 0];
      for(let i = 0; i < turnChangeResponse.costs.length; i++){
        if(turnChangeResponse.costs[i].name == "ability"){
          costsArrived[0] = turnChangeResponse.costs[i].value;
        } else if(turnChangeResponse.costs[i].name == "power") {
          costsArrived[1] = turnChangeResponse.costs[i].value;
        } else if(turnChangeResponse.costs[i].name == "tactic") {
          costsArrived[2] = turnChangeResponse.costs[i].value;
        } else if(turnChangeResponse.costs[i].name == "tool") {
          costsArrived[3] = turnChangeResponse.costs[i].value;
        }
      }
      this.costService.sendActualCosts(costsArrived);
      this.turnChangePrePhaseService.turnChangePrePhaseResponseHandler(
        turnChangeResponse, 
        this.characters, 
        this.enemyCharacters, 
        this.friendlyCharacterBuffs, 
        this.enemyCharacterBuffs
        );
      this.gameStateService.ownTheRound = !this.gameStateService.ownTheRound;
      this.ownTheRound = this.gameStateService.ownTheRound;
      this.gameStateService.sendGameOwnerChange(this.gameStateService.ownTheRound);
      
    });
    this.gameMenuClickService.turnChangeContinued$.subscribe(value => {
      for (let i = 0; i < this.friendlyCharacterBuffs.length; i++) {
        this.friendlyCharacterBuffs[i] = this.buffOrderService.reOrder(this.friendlyCharacterBuffs[i], value);
      }
      for (let i = 0; i < this.enemyCharacterBuffs.length; i++) {
        this.enemyCharacterBuffs[i] = this.buffOrderService.reOrder(this.enemyCharacterBuffs[i], value);
      }
      this.setAllSkillDisabled();
      this.setNotUndoAbleAllBuffs();
      this.ownTheRound = false;
    });
    this.costService.actualCosts$.pipe(delay(0)).subscribe(value =>{
      this.checkAvailableSkillsByCosts(value);
      if(!this.ownTheRound){
        this.setAllSkillDisabled();
      }
      this.checkCooldowns();
    });
    this.gameStateService.gameOwnerChange$.subscribe(value => {
      if(value){
        this.costService.sendRequest();
      }
      this.ownTheRound = value;
    });
    this.gameStateService.gameGivenToTheEnemy$.subscribe(value => {
      this.setAllSkillStateToNothing();
    });
    this.gameMenuClickService.exitTheGame$.subscribe(()=>{
      this.webSocketService.disconnect();
      window.location.reload();
    });
    setTimeout(this.costService.sendRequest());
    setTimeout(()=>{
      if(!this.ownTheRound){
        this.gameStateService.sendGameOwnerChange(false);
      }else{
        this.gameStateService.sendGameOwnerChange(true);
      }
    });
  }

  private checkAvailableSkillsByCosts(value: number[]): void {
    for (let i = 0; i < this.characters.length; i++) {
      this.checkAvailableSkillsByCostsOnOneCharacter(value, i);
    }
  }

  private checkAvailableSkillsByCostsOnOneCharacter(value: number[], i: number): void{
    let hasNoUsedSkillOnCharacter = !this.characterHasUsedInRoundSkill(this.characters[i]);
    for (let j = 0; j < this.characters[i].skills.length; j++) {
      if (this.characters[i].skills[j].state == "NOTHING" && hasNoUsedSkillOnCharacter) {
        let costs: number[] = this.costService.getCosts(this.characters[i].skills[j]);
        let hasEnoughCost: boolean = true;
        let allCosts: number = value[0] + value[1] + value[2] + value[3] - this.costService.getUsedInRoundRandomMemory();

        /**Itt azt vizsgáljuk, hogy az összes skill cost van-e annyi, mint ami összesen van a usernek, levéve a használt
         * nocosttal
         */
        if (allCosts >= costs[0] + costs[1] + costs[2] + costs[3]) {
          for (let k = 0; k < value.length; k++) {
            /**Itt egyesével nézzük meg a costokat hogy megvannak-e */
            if (value[k] < costs[k]) {
              hasEnoughCost = false;
            }
          }
        } else {
          hasEnoughCost = false;
        }

        /**Itt azt nézzük, hogy a nocosttal is megvan-e a kellő mennyiség */
        if (costs[0] + costs[1] + costs[2] + costs[3] + costs[4] > allCosts) {
          hasEnoughCost = false;
        }
        if (hasEnoughCost == false) {
          this.skillAvailabilities[i][j] = true;
        } else {
          this.skillAvailabilities[i][j] = false;
        }
      } else if (this.characters[i].skills[j].state == "USED_IN_ROUND") {
        this.skillAvailabilities[i][j] = true;
      }
    }
  }

  private getASkillPositionOnBoard(skill: Skill): number[]{
    for(let i=0;i<this.characters.length;i++){
      for(let j=0;j<this.characters[i].skills.length;j++){
        if(this.characters[i].skills[j].id == skill.id){
          return [i, j];
        }
      }
    }
    return null;
  }

  private setAllTheOtherSkillDisabled(skill: Skill): void{
    let skillPosition: number[] = this.getASkillPositionOnBoard(skill);
    for(let i=0;i<this.skillAvailabilities.length;i++){
      for(let j=0;j<this.skillAvailabilities[i].length;j++){
        if(!(i == skillPosition[0] && j == skillPosition[1])){
          this.skillAvailabilities[i][j] = true;
        }
      }
    }
  }

  private characterHasUsedInRoundSkill(character: Character): boolean{
    let result: boolean = false;
    for(let i=0;i<character.skills.length;i++){
      if(character.skills[i].state == "USED_IN_ROUND"){
        return true;
      }
    }
    return result;
  }

  private checkAllSkillStateAndSet(): void{
    for(let i=0;i<this.characters.length;i++){
      if(this.characterHasUsedInRoundSkill(this.characters[i])){
        /**Ebben az esetben van used_in_round skillje a féregnek, ezért minden skill true. */
        this.setAllSkillDisabledOnOneCharacterByIndex(i);
      }else{
        this.checkAvailableSkillsByCostsOnOneCharacter(this.costService.getActualCostsMemoryContent(), i); 
      }
    }
  }

  private setNotUndoAbleAllBuffs(): void{
    for (let i = 0; i < this.friendlyCharacterBuffs.length; i++) {
      for (let j = 0; j < this.friendlyCharacterBuffs[i].length; j++) {
        this.friendlyCharacterBuffs[i][j].undoAble = false;
      }
    }
    for (let i = 0; i < this.enemyCharacterBuffs.length; i++) {
      for (let j = 0; j < this.enemyCharacterBuffs[i].length; j++) {
        this.enemyCharacterBuffs[i][j].undoAble = false;
      }
    }
  }

  private setAllSkillDisabled(): void{
    for(let i=0;i<this.characters.length;i++) {
      this.setAllSkillDisabledOnOneCharacterByIndex(i);
    }
  }

  private setAllSkillDisabledOnOneCharacterByIndex(characterIndex: number): void{
    for(let i=0;i<this.characters[characterIndex].skills.length;i++){
      this.skillAvailabilities[characterIndex][i] = true;
    }
  }

  public getHealtBarColor(actualHealth: number): string {
    return this.healthBarService.getHealtBarColor(actualHealth);
  }

  public getHealthBarWidth(actualHealth: number, maxHealth: number): number {
    return this.healthBarDefaultWidth / maxHealth * actualHealth;
  }

  private setAllSkillStateToNothing(): void{
    for(let i=0;i<this.characters.length;i++){
      let character = this.characters[i];
      let skills = character.skills;
      for(let j=0;j<skills.length;j++){
        let skill = skills[j];
        skill.state = 'NOTHING';
      }
    }
  }

}
