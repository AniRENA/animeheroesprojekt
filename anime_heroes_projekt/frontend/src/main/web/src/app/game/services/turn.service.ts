import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class TurnService {

  private url = environment.apiBase + '/turnChangePrePhase';

  turnChangePrePhaseResponseArrive = new Subject<TurnChangeResponse>();

  turnChangePrePhaseResponseArrive$ = this.turnChangePrePhaseResponseArrive.asObservable();

  constructor(private http: HttpClient) { }

  public changeTurnPrePhase(usedSkills: Skill[]): Observable<TurnChangeResponse> {
    return this.http.post<any>(this.url, {"usedSkills" : usedSkills});
  }

  public sendTurnChangePrePhaseResponseArrive(response: TurnChangeResponse): void{
    this.turnChangePrePhaseResponseArrive.next(response);
  }
}
