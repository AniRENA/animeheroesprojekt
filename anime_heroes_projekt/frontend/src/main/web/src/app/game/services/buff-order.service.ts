import { Injectable } from '@angular/core';
import { CharacterBuff } from '../dto/CharacterBuff';
import { ArrayService } from '../../services/array.service';
import { CostService } from './cost.service';

@Injectable()
export class BuffOrderService {

  constructor(
    private arrayService: ArrayService,
    private costService: CostService
  ) { }

  public reOrder(characterBuff: CharacterBuff[], reorderedSkills: Skill[]): CharacterBuff[] {
    let ownedReorderedSkills: Skill[] = this.getOwnedReorderedSkills(characterBuff, reorderedSkills);
    for (let i = 0; i < characterBuff.length; i++) {
      if (!this.isContainedInReorderedSkills(characterBuff[i], reorderedSkills)) {
        let index = this.indexOf(characterBuff[i], characterBuff);
        ownedReorderedSkills = this.arrayService.addInIndex(ownedReorderedSkills, characterBuff[i].skill, index);
      }
    }
    return this.createCharacterBuffArrayFromSkillArray(ownedReorderedSkills);
  }

  private createCharacterBuffArrayFromSkillArray(ownedReorderedSkills: Skill[]): CharacterBuff[] {
    let result: CharacterBuff[] = [];
    for (let i = 0; i < ownedReorderedSkills.length; i++) {
      let buff: CharacterBuff = new CharacterBuff();
      buff.skill = ownedReorderedSkills[i];
      buff.costs = this.costService.getCosts(ownedReorderedSkills[0]);
      buff.undoAble = true;
      result = this.arrayService.addLast(result, buff);
    }
    return result;
  }

  private indexOf(element: CharacterBuff, characterBuffs: CharacterBuff[]): number {
    for (let i = 0; i < characterBuffs.length; i++) {
      if (characterBuffs[i].skill.id == element.skill.id) {
        return i;
      }
    }
    return -1;
  }

  private isContainedInReorderedSkills(element: CharacterBuff, array: Skill[]) {
    for (let i = 0; i < array.length; i++) {
      if (array[i].id == element.skill.id) {
        return true;
      }
    }
    return false;
  }

  private getOwnedReorderedSkills(characterBuff: CharacterBuff[], reorderedSkills: Skill[]): Skill[] {
    return reorderedSkills.filter(element => {
      for (let i = 0; i < characterBuff.length; i++) {
        if (element.id == characterBuff[i].skill.id) {
          return true;
        }
      }
    });
  }

}
