import { Injectable } from '@angular/core';
import { CharacterBuff } from '../dto/CharacterBuff';
import { CostService } from './cost.service';
import { ArrayService } from '../../services/array.service';
import { EndGameService } from './end-game.service';

@Injectable()
export class TurnChangePrePhaseService {

  constructor(
    private costService: CostService,
    private arrayService: ArrayService,
    private endGameService: EndGameService
    ) { }

    /**TODO ide jön majd be a válasz, és ide kell majd meggérkezzen a karakterek saját skiljjeinek leírása is, hiszen
     * azokat is aktualizálni kell, vltozhat a cost, a cd, stb.....
     */
  public turnChangePrePhaseResponseHandler(
    response: TurnChangeResponse, 
    characters: Character[], 
    enemyCharacters: Character[],
    friendlyCharacterBuffs: CharacterBuff[][],
    enemyCharacterBuffs: CharacterBuff[][]
    ): void {  
    let userLoose = response.characters.every(character => character.actualHealth <= 0);
    let enemyLoose = response.enemyCharacters.every(character => character.actualHealth <= 0);
    if(userLoose){
      this.endGameService.sendGameOver(true);
    }
    if(enemyLoose){
      this.endGameService.sendGameOver(false);
    }
    for(let i = 0; i<characters.length;i++) {
      for(let j = 0; j<response.characters.length;j++){
        if(characters[i].name == response.characters[j].name) {
          characters[i].name = response.characters[j].name;
          characters[i].imageName = response.characters[j].imageName;
          characters[i].health = response.characters[j].health;
          characters[i].actualHealth = response.characters[j].actualHealth;
          characters[i].item = response.characters[j].item;
          friendlyCharacterBuffs[this.findCharacterIndex(characters[i], characters)] = this.createAllBuffFromSkills(response.characters[j].buffs);
          for(let k=0;k<characters[i].skills.length;k++){
            for(let l=0;l<response.characters[j].skills.length;l++){
              if(characters[i].skills[k].name == response.characters[j].skills[l].name){
                /**TODO itt még a többi attribútumot is be kell állítanunk */
                characters[i].skills[k].actualCooldown = response.characters[j].skills[l].actualCd;
              }
            }
          }
        }
      }
    }

    for(let i=0;i<enemyCharacters.length;i++) {
      for(let j = 0; j<response.enemyCharacters.length;j++){
        if(enemyCharacters[i].name == response.enemyCharacters[j].name) {
          enemyCharacters[i].name = response.enemyCharacters[j].name;
          enemyCharacters[i].imageName = response.enemyCharacters[j].imageName;
          enemyCharacters[i].health = response.enemyCharacters[j].health;
          enemyCharacters[i].actualHealth = response.enemyCharacters[j].actualHealth;
          enemyCharacters[i].item = response.enemyCharacters[j].item;
          enemyCharacterBuffs[this.findCharacterIndex(enemyCharacters[i], enemyCharacters)] = this.createAllBuffFromSkills(response.enemyCharacters[j].buffs);
        }
      }
    }
  }

  private findCharacterIndex(character: Character, characters: Character[]): number {
    for(let i = 0; i<characters.length;i++) {
      if(character.name == characters[i].name) {
        return i;
      }
    }
  }

  private createAllBuffFromSkills(skills: Skill[]): CharacterBuff[] {
    let buffs: CharacterBuff[] = [];
    for(let i=0;i<skills.length;i++) {
      buffs = this.arrayService.addLast(buffs, this.createBuffFromSkill(skills[i]));
    }
    return buffs;
  }

  private createBuffFromSkill(skill: Skill): CharacterBuff {
    let buff: CharacterBuff  = new CharacterBuff();
        buff.skill = skill;
        buff.costs = this.costService.getCosts(skill);
        buff.undoAble = false;
        return buff;
  }
}
