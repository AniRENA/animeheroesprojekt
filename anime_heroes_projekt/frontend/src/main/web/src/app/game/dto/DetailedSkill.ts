/**Ez az osztály a db-ben is leírt skilleket írja le, tehát részletesen minden benne van, ez körváltásokkor fontos
 * , ez alapján aktualizálja a kliens az állapotokat.  */
class DetailedSkill {
     cd: number;
     effects: string[];
     img: string;
     cost:string[];
     actualCd: number;
     actualCost: string[];
     name: string;
     description: string;
     classes: string[];
     targetsOfSkill:string;
}
