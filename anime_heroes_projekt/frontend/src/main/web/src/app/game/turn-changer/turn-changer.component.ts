import { Component, OnInit } from '@angular/core';
import { GameMenuClickService } from '../services/game-menu-click.service';
import { UsedSkillsService } from '../services/used-skills.service';
import { CostService } from '../services/cost.service';
import { GameStateService } from '../services/game-state.service';

@Component({
  selector: 'app-turn-changer',
  templateUrl: './turn-changer.component.html',
  styleUrls: ['./turn-changer.component.css']
})
export class TurnChangerComponent implements OnInit{

  public usedSkills: Skill[] = [];

  public visibleFirstIndex: number = 0;

  public visibleLastIndex: number = 3;

  public actualCostsMemory: number[];

  public exchangedCosts: number[] = [0, 0, 0, 0];

  public actualRandomUsed: number = 0;

  public actualRandomMustUseFinalVariable = 0;

  constructor(
    private gameMenuClickService: GameMenuClickService, 
    private usedSkillsService: UsedSkillsService,
    private costService: CostService,
    private gameStateService: GameStateService
    ) { }

  ngOnInit(){
    this.usedSkills = this.usedSkillsService.getUsedSkills();
    this.actualRandomUsed = this.costService.getUsedInRoundRandomMemory();
    this.actualCostsMemory = JSON.parse(JSON.stringify(this.costService.getActualCostsMemoryContent()));
    this.actualRandomMustUseFinalVariable = this.actualRandomUsed;
    this.gameStateService.gameGivenToTheEnemy$.subscribe(value => {
      this.usedSkillsService.clearUsedSkills();
      this.usedSkills = [];
    });
  }

  public isSkillVisible(index: number): boolean{
    return index >= this.visibleFirstIndex && index <= this.visibleLastIndex; 
  }

  public onBackwardHandler(): void{
    if(this.visibleLastIndex < this.usedSkills.length - 1){
      this.visibleLastIndex++;
      this.visibleFirstIndex++;
    }
  }

  public onForwardHandler(): void{
    if(this.visibleFirstIndex > 0){
      this.visibleLastIndex--;
      this.visibleFirstIndex--;
    }
  }

  public okayHandler(): void{
    if(this.actualRandomUsed == 0){
      this.gameMenuClickService.sendChangeTurnContinued(this.usedSkills);
    }
  }

  public cancelHandler(): void{
    this.gameMenuClickService.sendChangeTurnCanceled();
  }

  public abilityIncreaseHandler(): void{
    if(this.actualRandomUsed > 0 && this.actualCostsMemory[0] > this.exchangedCosts[0]){
      this.exchangedCosts[0] = this.exchangedCosts[0] + 1;
      this.actualCostsMemory[0] = this.actualCostsMemory[0] - 1;
      this.actualRandomUsed--;
    }
  }

  public abilityDecreaseHandler(): void{
    if(this.exchangedCosts[0] > 0 && this.actualRandomUsed < this.actualRandomMustUseFinalVariable){
      this.exchangedCosts[0] = this.exchangedCosts[0] - 1;
      this.actualCostsMemory[0] = this.actualCostsMemory[0] + 1;
      this.actualRandomUsed++;
    }
  }

  public powerIncreaseHandler(): void{
    if(this.actualRandomUsed > 0 && this.actualCostsMemory[1] > this.exchangedCosts[1]){
      this.exchangedCosts[1] = this.exchangedCosts[1] + 1;
      this.actualCostsMemory[1] = this.actualCostsMemory[1] - 1;
      this.actualRandomUsed--;
    }
  }

  public powerDecreaseHandler(): void{
    if(this.exchangedCosts[1] > 0 && this.actualRandomUsed < this.actualRandomMustUseFinalVariable){
      this.exchangedCosts[1] = this.exchangedCosts[1] - 1;
      this.actualCostsMemory[1] = this.actualCostsMemory[1] + 1;
      this.actualRandomUsed++;
    }
  }

  public tactiIncreaseHandler(): void{
    if(this.actualRandomUsed > 0 && this.actualCostsMemory[2] > this.exchangedCosts[2]){
      this.exchangedCosts[2] = this.exchangedCosts[2] + 1;
      this.actualCostsMemory[2] = this.actualCostsMemory[2] - 1;
      this.actualRandomUsed--;
    }
  }

  public tacticDecreaseHandler(): void{
    if(this.exchangedCosts[2] > 0 && this.actualRandomUsed < this.actualRandomMustUseFinalVariable){
      this.exchangedCosts[2] = this.exchangedCosts[2] - 1;
      this.actualCostsMemory[2] = this.actualCostsMemory[2] + 1;
      this.actualRandomUsed++;
    }
  }

  public toolIncreaseHandler(): void{
    if(this.actualRandomUsed > 0 && this.actualCostsMemory[3] > this.exchangedCosts[3]){
      this.exchangedCosts[3] = this.exchangedCosts[3] + 1;
      this.actualCostsMemory[3] = this.actualCostsMemory[3] - 1;
      this.actualRandomUsed--;
    }
  }

  public toolDecreaseHandler(): void{
    if(this.exchangedCosts[3] > 0 && this.actualRandomUsed < this.actualRandomMustUseFinalVariable){
      this.exchangedCosts[3] = this.exchangedCosts[3] - 1;
      this.actualCostsMemory[3] = this.actualCostsMemory[3] + 1;
      this.actualRandomUsed++;
    }
  }
}
