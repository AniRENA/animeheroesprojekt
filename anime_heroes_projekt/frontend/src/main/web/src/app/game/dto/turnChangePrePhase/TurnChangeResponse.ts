interface TurnChangeResponse {
    enemyCharacters: TurnChangeCharacter[];
    characters: TurnChangeCharacter[];
    costs: Cost[];
}