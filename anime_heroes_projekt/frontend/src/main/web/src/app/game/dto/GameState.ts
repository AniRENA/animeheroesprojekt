interface GameState {
	find: boolean;
	startsTheGame: boolean;
	enemyUser: UserDetail;
	enemyCharacters: Character[];
	user: UserDetail;
	characters: Character[];
	costs: Cost[];
}