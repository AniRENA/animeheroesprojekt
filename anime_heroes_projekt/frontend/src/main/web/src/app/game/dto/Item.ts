interface Item {
    name: string;

    /*Két féle item létezik, active és passzív */
    isActive: boolean;
    imageName: string;
    classes: string[];
    costs: string[];
    state: string;/**NOTHING, WANT_TO_USE, USED_IN_ROUND */
    whoCanBeATarget: number[]; //ebből kiderül, hogy kire rakhatta, és hogy kikre kell, hogy hatással legyen
    cooldown: number;
    description: string;
}