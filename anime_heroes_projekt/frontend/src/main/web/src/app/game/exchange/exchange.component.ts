import { Component, OnInit } from '@angular/core';
import { GameMenuClickService } from '../services/game-menu-click.service';
import { CostService } from '../services/cost.service';

@Component({
  selector: 'app-exchange',
  templateUrl: './exchange.component.html',
  styleUrls: ['./exchange.component.css']
})
export class ExchangeComponent implements OnInit{

  /*Alapértelmezetten 5 cost-ért lehet beválltani egy tetszőleges costra. */
  public exchangeCostFinal: number = 5;

  public actualExchangeCost: number = 5;

  public actualCostsMemory: number[];

  public exchangedCosts: number[] = [0, 0, 0, 0, 0];

  public costsWidths: number[] = [10, 10, 10, 10];

  public selectedCostIndex: number = -1;

  constructor(
    private gameMenuClickService: GameMenuClickService,
    private costService: CostService
    ) { }

  ngOnInit(){
    this.actualCostsMemory = JSON.parse(JSON.stringify(this.costService.getActualCostsMemoryContent()));
  }

  public continueHandler(): void{
    this.gameMenuClickService.sendExchangeContinued();
  }

  public abilityIncreaseHandler(): void{
    if(this.actualExchangeCost > 0 && this.actualCostsMemory[0] > 0){
      this.exchangedCosts[0] = this.exchangedCosts[0] + 1;
      this.actualCostsMemory[0] = this.actualCostsMemory[0] - 1;
      this.actualExchangeCost--;
    }
  }

  public abilityDecreaseHandler(): void{
    if(this.exchangedCosts[0] > 0 && this.exchangedCosts[0] > 0){
      this.exchangedCosts[0] = this.exchangedCosts[0] - 1;
      this.actualCostsMemory[0] = this.actualCostsMemory[0] + 1;
      this.actualExchangeCost++;
    }
  }

  public powerIncreaseHandler(): void{
    if(this.actualExchangeCost > 0 && this.actualCostsMemory[1] > 0){
      this.exchangedCosts[1] = this.exchangedCosts[1] + 1;
      this.actualCostsMemory[1] = this.actualCostsMemory[1] - 1;
      this.actualExchangeCost--;
    }
  }

  public powerDecreaseHandler(): void{
    if(this.exchangedCosts[1] > 0 && this.exchangedCosts[1] > 0){
      this.exchangedCosts[1] = this.exchangedCosts[1] - 1;
      this.actualCostsMemory[1] = this.actualCostsMemory[1] + 1;
      this.actualExchangeCost++;
    }
  }

  public tactiIncreaseHandler(): void{
    if(this.actualExchangeCost > 0 && this.actualCostsMemory[2] > 0){
      this.exchangedCosts[2] = this.exchangedCosts[2] + 1;
      this.actualCostsMemory[2] = this.actualCostsMemory[2] - 1;
      this.actualExchangeCost--;
    }
  }

  public tacticDecreaseHandler(): void{
    if(this.exchangedCosts[2] > 0 && this.exchangedCosts[2] > 0){
      this.exchangedCosts[2] = this.exchangedCosts[2] - 1;
      this.actualCostsMemory[2] = this.actualCostsMemory[2] + 1;
      this.actualExchangeCost++;
    }
  }

  public toolIncreaseHandler(): void{
    if(this.actualExchangeCost > 0 && this.actualCostsMemory[3] > 0){
      this.exchangedCosts[3] = this.exchangedCosts[3] + 1;
      this.actualCostsMemory[3] = this.actualCostsMemory[3] - 1;
      this.actualExchangeCost--;
    }
  }

  public toolDecreaseHandler(): void{
    if(this.exchangedCosts[3] > 0 && this.exchangedCosts[3] > 0){
      this.exchangedCosts[3] = this.exchangedCosts[3] - 1;
      this.actualCostsMemory[3] = this.actualCostsMemory[3] + 1;
      this.actualExchangeCost++;
    }
  }

  public costClickhandler(index: number): void{
    this.costsWidths[0] = 10;
    this.costsWidths[1] = 10;
    this.costsWidths[2] = 10;
    this.costsWidths[3] = 10;
    this.costsWidths[index] = 20;
    this.selectedCostIndex = index;
  }

  public okayHandler(): void{
    if(this.actualExchangeCost == 0 && this.selectedCostIndex != -1){
      let changeCosts: number[] = [0, 0, 0, 0, 0];
      changeCosts[this.selectedCostIndex] = changeCosts[this.selectedCostIndex] + 1;

      this.costService.changeCostsByDifference(this.exchangedCosts);
      this.costService.addChangeCost(changeCosts);
      this.gameMenuClickService.sendExchangeContinued();
    }
  }

  public cancelHandler(): void{
    this.gameMenuClickService.sendExchangeCanceled();
  }

}
