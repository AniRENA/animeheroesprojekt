import { Component, OnInit } from '@angular/core';
import { GameMenuClickService } from '../services/game-menu-click.service';

@Component({
  selector: 'app-surrender',
  templateUrl: './surrender.component.html',
  styleUrls: ['./surrender.component.css']
})
export class SurrenderComponent{

  constructor(private gameMenuClickService: GameMenuClickService) { }

  public onOkHandler(): void{
    this.gameMenuClickService.sendSurrenderClickAction(true);
  }

  public onCancelHandler(): void{
    this.gameMenuClickService.sendSurrenderClickAction(false);
  }

}
