import { Injectable } from '@angular/core';

@Injectable()
export class HealthBarService {

  constructor() { }

  public getHealtBarColor(actualHealth: number): string {
    if(actualHealth > 80) {
      return "green";
    } else if(actualHealth > 40) {
      return "yellow";
    } else {
      return "red";
    }
  }

}
