interface Character {
    name: string;
    imageName: string;
    skills: Skill[];
    item: Item;
    health: number;
    actualHealth: number;
}