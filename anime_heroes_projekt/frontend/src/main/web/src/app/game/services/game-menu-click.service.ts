import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { ReplaySubject } from 'rxjs';

@Injectable()
export class GameMenuClickService {

  surrenderClicked = new Subject<boolean>();

  surrenderClicked$ = this.surrenderClicked.asObservable();

  loseTheGame = new ReplaySubject<boolean>();

  loseTheGame$ = this.loseTheGame.asObservable();

  exchangeClicked = new Subject<boolean>();

  exchangeClicked$ = this.exchangeClicked.asObservable();

  turnChangeClicked = new Subject<boolean>();

  turnChangeClicked$ = this.turnChangeClicked.asObservable();

  exchangeContinued = new Subject<boolean>();

  exchangeContinued$ = this.exchangeContinued.asObservable();

  exchangeCanceled = new Subject<boolean>();

  exchangeCanceled$ = this.exchangeCanceled.asObservable();

  turnChangeContinued = new Subject<Skill[]>();

  turnChangeContinued$ = this.turnChangeContinued.asObservable();

  turnChangeCancel = new Subject<boolean>();

  turnChangeCancel$ = this.turnChangeCancel.asObservable();

  exitTheGame = new Subject<void>();

  exitTheGame$ = this.exitTheGame.asObservable();

  constructor() { }

  public sendSurrenderClickAction(param: boolean): void{
    this.surrenderClicked.next(param);
  }

  public sendLoseTheGameAction(param: boolean): void{
    this.loseTheGame.next(param);
  }

  public sendExchangeAction(): void{
    this.exchangeClicked.next(true);
  }

  public sendTurnChangeAction(): void{
    this.turnChangeClicked.next(true);
  }

  /*TODO ez a metódus, majd azt fogja kapni még extrában. hogy miket cserélt be a user(milyen costokat). */
  public sendExchangeContinued(): void{
    this.exchangeContinued.next(true);
  }

  public sendExchangeCanceled(): void{
    this.exchangeCanceled.next(true);
  }

  /*TODO ez a metódus fogja triggerelni a körátadást, és több paramétert kell kapnia az ablaktól. */
  public sendChangeTurnContinued(usedSkills: Skill[]): void{
    this.turnChangeContinued.next(usedSkills);
  }

  public sendChangeTurnCanceled(): void{
    this.turnChangeCancel.next(true);
  }

  public sendExitTheGame(): void{
    this.exitTheGame.next();
  }

}
