import { Component, OnInit } from '@angular/core';
import { DescriptionService } from '../services/description.service';

@Component({
  selector: 'app-character-descriptor',
  templateUrl: './character-descriptor.component.html',
  styleUrls: ['./character-descriptor.component.css']
})
export class CharacterDescriptorComponent implements OnInit {

  public skill: Skill;

  constructor(private descriptionService: DescriptionService) { }

  ngOnInit() {
    this.descriptionService.selectedSkill$.subscribe(skill =>{
      this.skill = skill;
    });
  }

  public isNoCostTitleNeeded(): boolean{
    if(this.skill && this.skill.costs.length == 0){
      return true;
    }
    return false;
  }

  public getCooldownValue(value: number): string{
    if(value == 0){
      return "INFINITE"
    }else if(value < 0){
      return "NONE";
    }else {
      return value.toString();
    }
  }

}
