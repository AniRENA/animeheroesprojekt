import { Injectable } from '@angular/core';
import { ArrayService } from '../../services/array.service';

@Injectable()
export class UsedSkillsService {

  private usedSkills: Skill[] = [];

  constructor(private arrayService: ArrayService) { }

  addNewlyUsedSkill(skill: Skill): void{
    this.arrayService.addLast(this.usedSkills, skill);
  }

  removeFromUsedSkills(skill: Skill): void{
    let indexOfRemovingSkill = -1;
      for(let i=0; i< this.usedSkills.length; i++) {
        if(this.usedSkills[i].name == skill.name) {
          indexOfRemovingSkill = i;
        }
      }
      if(indexOfRemovingSkill != -1) {
        this.arrayService.remove(this.usedSkills, indexOfRemovingSkill);
      }
  }

  getUsedSkills(): Skill[] {
    return this.usedSkills;
  }

  clearUsedSkills(): void{
    this.usedSkills = [];
  }

}
