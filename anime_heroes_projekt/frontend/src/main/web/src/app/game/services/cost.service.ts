import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { ReplaySubject } from 'rxjs/ReplaySubject';

@Injectable()
export class CostService {

  getCostsRequest = new ReplaySubject<boolean>();

  getCostsRequest$ = this.getCostsRequest.asObservable();

  actualCosts = new Subject<number[]>();

  actualCosts$ = this.actualCosts.asObservable();

  changeCosts = new Subject<number[]>();

  changeCosts$ = this.changeCosts.asObservable();

  addChangeCosts = new Subject<number[]>();

  addChangeCosts$ = this.addChangeCosts.asObservable();

  private actualCostsMemory: number[];/** Ebben a tömbben tároljuk az aktuálisan lévő costokat(csak a 4 színes costot) */

  private usedInRoundRandomMemory: number = 0;/**Ez minden körben nullázódik, azt a pozitív számot tárolja, ahány nocost random
  lett használva */

  constructor() { }

  public sendRequest(): void{
    this.getCostsRequest.next(true);
  }

  public sendActualCosts(costs: number[]): void{
    this.actualCostsMemory = costs;
    this.actualCosts.next(costs);
  }

  public changeCostsByDifference(costsDifference: number[]): void{
    this.changeCosts.next(costsDifference);
  }

  public addChangeCost(costsDifference: number[]): void{
    this.addChangeCosts.next(costsDifference);
  }

  public getActualCostsMemoryContent(): number[]{
    return this.actualCostsMemory;
  }

  public setUsedInRoundRandomMemory(value: number): void{
    this.usedInRoundRandomMemory = value;
  }

  public getUsedInRoundRandomMemory(): number{
    return this.usedInRoundRandomMemory;
  }

  public getCosts(skill: Skill): number[]{
    let result: number[] = [0, 0, 0, 0, 0];
    /*ability power tactic tool a sorrend*/
    for(let i=0;i<skill.costs.length;i++){
      if(skill.costs[i] == "ability") {
        result[0]++;
      }else if(skill.costs[i] == "power"){
        result[1]++;
      }else if(skill.costs[i] == "tactic"){
        result[2]++;
      }else if(skill.costs[i] == "tool"){
        result[3]++;
      }else if(skill.costs[i] == "random"){
        result[4]++;
      }
    }
    return result;
  }
}
