import { Injectable } from '@angular/core';
import * as Stomp from 'stompjs';

@Injectable()
export class WebsocketService {

  private ws: any;

  constructor() { }

  public connect(successCallback: (message: any) => any): void {
    let socket = new WebSocket("ws://localhost:8888/greeting");
    this.ws = Stomp.over(socket);
    let that = this;
    this.ws.connect({}, function(frame: any) {
      that.ws.subscribe("/user/queue/errors", function(message: any) {
        alert("Error " + message.body);
      });
      that.ws.subscribe("/user/queue/reply", successCallback);
    }, function(error: any) {
      console.log("STOMP error " + error);
    });
  }

  public send(data: any): void{
    this.ws.send("/app/message", {}, data);
  }

  public disconnect(): void {
    if (this.ws != null) {
      this.ws.ws.close();
    }
    console.log("Disconnected");
  }
}
