
class Skill {
    id: number;
    name: string;
    description: string;
    imageName: string;
    classes: string[];
    costs: string[];
    state: string;/**NOTHING, WANT_TO_USE, USED_IN_ROUND */
    whoCanBeATarget: number[]; //ebből kiderül, hogy kire rakhatta, és hogy kikre kell, hogy hatással legyen
    whoIsTheTarget: number; //ez azt mondja meg, hogy konkrétan kire rakta a skillt! 0,1,2 -> sajátok, 3,4,5 -> ellenségek
    cooldown: number;
    actualCooldown: number;
}