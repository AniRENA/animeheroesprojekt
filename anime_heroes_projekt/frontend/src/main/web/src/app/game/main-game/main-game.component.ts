import { Component, OnInit, Input } from '@angular/core';
import { GameMenuClickService } from '../services/game-menu-click.service';
import { SoundService } from '../../services/sound.service';
import { TurnService } from '../services/turn.service';
import { GameStateService } from '../services/game-state.service';
import { EndGameService } from '../services/end-game.service';

@Component({
  selector: 'app-main-game',
  templateUrl: './main-game.component.html',
  styleUrls: ['./main-game.component.css']
})
export class MainGameComponent implements OnInit{

  @Input()
  public gameState: GameState;

  public isSurrenderVisible: boolean = false;

  public isResultVisible: boolean = false;

  public isExchangeVisible: boolean = false;

  public isTurnChangerVisible: boolean = false;

  constructor(
    private gameMenuClickService: GameMenuClickService,
    private soundService: SoundService,
    private turnService: TurnService,
    private gameStateService: GameStateService,
    private endGameService: EndGameService
  ) { }

  ngOnInit(){
    this.gameMenuClickService.surrenderClicked$.subscribe(value => {
      this.isSurrenderVisible = false;
      if(value){
        this.isResultVisible = true;
        this.gameMenuClickService.sendLoseTheGameAction(true);
      }
    });
    this.endGameService.gameOver$.subscribe(value => {
      this.isResultVisible = true;
      this.gameMenuClickService.sendLoseTheGameAction(value);
    });
    this.gameMenuClickService.exchangeClicked$.subscribe(value => {
      this.isExchangeVisible = true;
    });
    this.gameMenuClickService.turnChangeClicked$.subscribe(value => {
      this.isTurnChangerVisible = true;
    });
    this.gameMenuClickService.turnChangeContinued$.subscribe(value => {
      this.isTurnChangerVisible = false;
      this.turnService.changeTurnPrePhase(value).subscribe(response => {
        this.turnService.sendTurnChangePrePhaseResponseArrive(response);
        this.gameStateService.sendGameGivenToTheEnemy();
      });
    });
    this.gameMenuClickService.exchangeContinued$.subscribe(value => {
      this.isExchangeVisible = false;
    });
    this.gameMenuClickService.turnChangeCancel$.subscribe(value => {
      this.isTurnChangerVisible = false;
    });
    this.gameMenuClickService.exchangeCanceled$.subscribe(value => {
      this.isExchangeVisible = false;
    });
  }

  public getCost(param: String): number{
    let c: Cost[] = this.gameState.costs.filter(cost => {
      if(cost.name === param){
        return cost;
      }
    });
    return c[0].value;
  }

  public surrenderClickHandler(): void{
    this.isSurrenderVisible = true;
  }

  public soundClickHandler(): void{
    this.soundService.canPlaySong = !this.soundService.canPlaySong;
  }

  public isSoundPlayAvailable(): boolean{
    return this.soundService.canPlaySong;
  }
}
