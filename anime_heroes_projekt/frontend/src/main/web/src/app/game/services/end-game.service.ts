import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class EndGameService {

  gameOver = new Subject<boolean>();

  gameOver$ = this.gameOver.asObservable();

  constructor() { }

  sendGameOver(isThisPlayerLoose: boolean): void{
    this.gameOver.next(isThisPlayerLoose);
  }

}
