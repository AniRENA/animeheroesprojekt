import { Injectable } from '@angular/core';

@Injectable()
export class RankService {

  constructor() { }

  public selectImageForRank(rank: string): string {
    if(rank == "NEWBIE_WARRIOR"){
      return "../../../assets/game/NEWBIE_WARRIOR.png";
    } else if(rank == "GHOUL") {
      return "../../../assets/game/GHOUL.png";
    } else if(rank == "HOBBY_HEROE") {
      return "../../../assets/game/Level26-30.png";
    } else if(rank == "REAPER") {
      return "../../../assets/game/REAPER.png";
    } else if(rank == "STRAW_HAT_PIRATE") {
      return "../../../assets/game/STRAW_HAT_PIRATE.png";
    } else if(rank == "AIRBENDER") {
      return "../../../assets/game/AIRBENDER.png";
    } else if(rank == "EXORCIST") {
      return "../../../assets/game/EXORCIST.png";
    }
    return "";
  }

  public getZindexOfRank(rank: string): number {
    if(rank == "NEWBIE_WARRIOR"){
      return 1;
    } else if(rank == "GHOUL") {
      return 30;
    } else if(rank == "HOBBY_HEROE") {
      return 30;
    } else if(rank == "REAPER") {
      return 1;
    } else if(rank == "STRAW_HAT_PIRATE") {
      return 30;
    } else if(rank == "AIRBENDER") {
      return 1;
    } else if(rank == "EXORCIST") {
      return 30;
    }
    return 30;
  }

}
