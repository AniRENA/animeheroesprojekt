import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
import { Subject } from 'rxjs';

@Injectable()
export class GameStateService {

  private url = environment.apiBase + '/searchEnemy';

  gameOwnerChange = new Subject<boolean>();

  gameOwnerChange$ = this.gameOwnerChange.asObservable();

  gameGivenToTheEnemy = new Subject<boolean>();

  gameGivenToTheEnemy$ = this.gameOwnerChange.asObservable();

  ownTheRound: boolean = false;

  constructor(private http: HttpClient) { }

  getGameState(selectedCharacters: SelectionCharacter[]): Observable<GameState>{
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Access-Control-Allow-Origin':  '*',
        'Access-Control-Allow-Methods': 'POST, GET, OPTIONS'
      })
    };
    return this.http.post<GameState>(this.url, {"characters": selectedCharacters}, httpOptions);
  }

  /**
   * Ezen keresztül lehet kommunikálni a komponensek felé, hogy most kör átadás történt.
   * True-val hívva megkapja a kört, falseal pedig elveszti.
   */
  sendGameOwnerChange(isThisClientOwnTheRound): void{
    this.ownTheRound = isThisClientOwnTheRound;
    this.gameOwnerChange.next(isThisClientOwnTheRound);
  }

  sendGameGivenToTheEnemy(): void{
    this.gameGivenToTheEnemy.next(true);
  }

}
