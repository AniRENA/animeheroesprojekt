enum SkillState{
    NOTHING,
    WANT_TO_USE,
    USED_IN_ROUND 
}
/*NOTHING -> Azt jelenti, hogy a körben még hozzá sem nyúlt a user
WANT_TO_USE -> Egyszer már rákattintott a skillre, ilyenkor kiszineződnek azok a karakterek, akiken lehet használni a 
skillt. Ha az egyik skill olyen állapotban van, akkor a többi skill állapota nem változtatható.
USED_IN_ROUND -> Ez azt jelenti, hogy ez a skill, ebben a körben már használva van, tehát mindenképp szürkének
kell lennie, hisz ebben a körben már nem használható. */

/*Ez az enum csak dokumentációs céllal létezik egyenlérőe */