interface TurnChangeCharacter {
    name: string;
    imageName: string;
    health: number;
    actualHealth: number;
    item: Item;
    buffs: Skill[];
    skills: DetailedSkill[];
}