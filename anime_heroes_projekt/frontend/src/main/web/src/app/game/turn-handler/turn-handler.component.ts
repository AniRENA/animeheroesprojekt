import { Component, Input } from '@angular/core';
import { CostService } from '../services/cost.service';
import { GameMenuClickService } from '../services/game-menu-click.service';
import { GameStateService } from '../services/game-state.service';
import { UsedSkillsService } from '../services/used-skills.service';

@Component({
  selector: 'app-turn-handler',
  templateUrl: './turn-handler.component.html',
  styleUrls: ['./turn-handler.component.css']
})
export class TurnHandlerComponent{

  @Input()
  public ability: number = 0;

  @Input()
  public power: number = 0;

  @Input()
  public tactic: number = 0;

  @Input()
  public tool: number = 0;

  public usedRandom: number = 0;

  public title: string = "YOUR TURN";

  private yourTurnTitle: string = "YOUR TURN";

  private opponentTurnTitle: string = "ENEMY TURN";

  private defaultActualTimeWidth: number = 208;

  public actualTimeWidth: number = 208;

  public isPlayerTurn: boolean = true;

  private timerId;

  private timeOutId;

  private milisecond: number = 1000;

  private roundTime: number = 30 * this.milisecond;

  constructor(
    private gameStateService: GameStateService,
    private costService: CostService,
    private gameMenuClickService: GameMenuClickService,
    private usedSkillsService: UsedSkillsService
  ) { }

  ngOnInit(){
    this.costService.setUsedInRoundRandomMemory(this.usedRandom);
    this.startTimeLine();
    this.costService.getCostsRequest$.subscribe(value => {
      this.costService.sendActualCosts(this.createArrayFromFliedCosts());
    });

    this.costService.actualCosts$.subscribe(value =>{
      this.ability = value[0];
      this.power = value[1];
      this.tactic = value[2];
      this.tool = value[3];
    });

    this.costService.changeCosts$.subscribe(value => {
      this.ability -= value[0];
      this.power -= value[1];
      this.tactic -= value[2];
      this.tool -= value[3];
      this.usedRandom += value[4];
      this.costService.sendActualCosts(this.createArrayFromFliedCosts());
      this.costService.setUsedInRoundRandomMemory(this.usedRandom);
    });

    this.costService.addChangeCosts$.subscribe(value => {
      this.ability += value[0];
      this.power += value[1];
      this.tactic += value[2];
      this.tool += value[3];
      this.usedRandom -= value[4];
      this.costService.sendActualCosts(this.createArrayFromFliedCosts());
      this.costService.setUsedInRoundRandomMemory(this.usedRandom);
    });

    this.gameMenuClickService.turnChangeContinued$.subscribe(value => {
      this.isPlayerTurn = false;
      this.gameStateService.ownTheRound = false;
      this.title = this.opponentTurnTitle;
      this.restartTimer();
    });

    this.gameStateService.gameOwnerChange$.subscribe(value => {
      this.restartTimer();
      this.isPlayerTurn = value;
      if(value){
        this.title = this.yourTurnTitle;
      }else{
        this.title = this.opponentTurnTitle;
      }
    });
  }

  ngOnDestroy(){
    clearInterval(this.timerId);
  }

  public onExchangeHandler(): void{
    if(this.title == this.yourTurnTitle) {
      this.gameMenuClickService.sendExchangeAction();
    } 
  }

  public onTurnChangeHandler(): void{
    if(this.title == this.yourTurnTitle) {
      this.gameMenuClickService.sendTurnChangeAction();
    }
  }

  public startTimeLine(): void{
 
    let periodTime: number = 1 * this.milisecond;
    let decreasePixels: number = this.defaultActualTimeWidth / (this.roundTime / this.milisecond);
    
    this.timerId = setInterval(() => {
      this.actualTimeWidth = this.actualTimeWidth - decreasePixels; 
    }, periodTime);
    this.timeOutHandlerSetup();
  }

  private restartTimer(): void{
    this.actualTimeWidth = this.defaultActualTimeWidth;
    clearTimeout(this.timeOutId);
    this.timeOutHandlerSetup();
  }

  private timeOutHandlerSetup(): void{
    this.timeOutId = setTimeout(() => { 
      /**Ide akkor futunk be, ha a kör aktív, és át kell adni, mert lejárt az idő! Zolival egyeztetni, hogy ilyenkor semmit küld fel,
       * vagy amiket használt a user, azt elküldjük-e.
       */
      console.log("Ide elvileg csak egyszer fut be!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
      if(this.title == this.yourTurnTitle) {
        this.gameMenuClickService.sendChangeTurnContinued(this.usedSkillsService.getUsedSkills());
      } 
    }, this.roundTime);
  }

  private createArrayFromFliedCosts(): number[]{
    return [this.ability, this.power, this.tactic, this.tool];
  }
}
