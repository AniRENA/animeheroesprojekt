import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class DescriptionService {

  selectedSkill = new Subject<Skill>();

  selectedSkill$ = this.selectedSkill.asObservable();

  constructor() { }

  public sendSelectedSkill(skill: Skill): void{
    this.selectedSkill.next(skill);
  }

}
