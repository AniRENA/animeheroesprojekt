import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {}

/*Ez a komponens csak azért van, hogy ez a bootstrapper, és ez töltődik be először. Neki van egy routing tage,
 amibe betölt komponenst. */