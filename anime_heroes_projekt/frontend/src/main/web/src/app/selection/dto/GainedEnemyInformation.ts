interface GainedEnemyInformation {
    find: boolean; 
	enemyUser: {
        userName: string,
        rank: string,
        clan: string,
        level: number,
        ladderRank: number,
        wins: number,
        loses: number,
        ratio: number
    };
    characters: {
        name: string,
        items: {
            name: string;
        }[]
    }[];
}
