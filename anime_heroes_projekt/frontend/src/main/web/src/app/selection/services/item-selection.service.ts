import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class ItemSelectionService {

  selectedItem = new Subject<SelectionItem>();

  selectedItem$ = this.selectedItem.asObservable();

  constructor() { }

  public sendSelectedItem(selectedItem: SelectionItem): void{
    this.selectedItem.next(selectedItem);
  }

}
