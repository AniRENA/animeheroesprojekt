import { Component, OnInit } from '@angular/core';
import { SelectionUserServiceService } from '../services/selection-user-service.service';

@Component({
  selector: 'app-selection-user-profile',
  templateUrl: './selection-user-profile.component.html',
  styleUrls: ['./selection-user-profile.component.css']
})
export class SelectionUserProfileComponent implements OnInit{

  public profileImg: string = "../../../assets/selection/Love.png";
  public selectionProfile: SelectionProfile;

  constructor(private selectionUserServiceService: SelectionUserServiceService) { }
  
  ngOnInit() {
    this.selectionUserServiceService.getProfile()
      .subscribe(profile => {this.selectionProfile = profile});
  }
}
