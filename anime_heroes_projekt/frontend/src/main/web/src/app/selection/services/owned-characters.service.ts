import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { environment } from '../../../environments/environment';

@Injectable()
export class OwnedCharactersService {

  private url = environment.apiBase + '/allCharacters';

  protected data: SelectionCharacter[] = null;
  protected subjectData: ReplaySubject<SelectionCharacter[]>;
  protected observableData: Observable<SelectionCharacter[]>;

  constructor(private http: HttpClient) {
    this.subjectData = new ReplaySubject(1);
    this.observableData = this.subjectData.asObservable(); 
  }

  public getData(): Observable<SelectionCharacter[]> {
    if (this.data == null) {
      this.getCharacters().subscribe(
        result => {
          this.subjectData.next(result);
          this.data = result;
          this.data.forEach(function(value){
            value.items = [null, null];
          });
        },
        err => this.subjectData.error(err)
      );
    }
    return this.observableData;
  }

  public resetCache(): void {
    this.data = null;
  }

  public refresh(): void {
    this.resetCache();
    this.getData();
  }

  private getCharacters(): Observable<SelectionCharacter[]> {
    let params = new HttpParams();
    params = params.append('var1', "alma");
    return this.http.get<SelectionCharacter[]>(this.url, {params: params});
  }
}

