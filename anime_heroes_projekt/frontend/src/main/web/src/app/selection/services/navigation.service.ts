import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class NavigationService {

  navigationRanked = new Subject<boolean>();

  navigationRanked$ = this.navigationRanked.asObservable();

  constructor() { }

  sendNavigationRanked(): void{
    this.navigationRanked.next(true);
  }

}
