import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class CharacterSelectionService {

  selectedCharacter = new Subject<SelectionCharacter>();

  selectedCharacter$ = this.selectedCharacter.asObservable();

  unSelectedCharacter = new Subject<string>();

  unSelectedCharacter$ = this.unSelectedCharacter.asObservable();

  sendSelectedCharacter(selectedCharacter: SelectionCharacter): void{
    this.selectedCharacter.next(selectedCharacter);
  }

  sendUnSelectedCharacter(unSelectedCharacterName: string): void{
    this.unSelectedCharacter.next(unSelectedCharacterName);
  }
}
