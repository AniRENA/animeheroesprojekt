interface SelectionCharacter {
    name: string; 
	img: string;
	owned: boolean;
	items: SelectionItem[];
}
