interface SelectionProfile {
    userName: string; 
	rank: string;
	clan: string;
	level: number;
	ladderRank: number;
	wins: number;
	loses: number;
	ratio: number;
}
