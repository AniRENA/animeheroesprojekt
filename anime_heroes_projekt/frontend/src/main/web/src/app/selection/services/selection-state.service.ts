import { Injectable } from '@angular/core';
import { SelectionState } from '../main-selection/selection-state';
import { Subject } from 'rxjs';

@Injectable()
export class SelectionStateService {

  private stateOfCharacterSelection: string = SelectionState.NOTREADY;

  event = new Subject<void>();

  event$ = this.event.asObservable();

  constructor() { }

  public sendEventFire(): void{
    this.event.next();
  }

  public getStateOfCharacterSelection(): string{
    return this.stateOfCharacterSelection;
  }

  public setStateOfCharacterSelection(value: string): void{
    this.stateOfCharacterSelection = value;
  }
}
