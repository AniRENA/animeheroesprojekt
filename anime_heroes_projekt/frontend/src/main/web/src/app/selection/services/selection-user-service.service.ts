import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';

@Injectable()
export class SelectionUserServiceService {

  private url = environment.apiBase + '/selectionProfile';

  constructor(private http: HttpClient) { }

  getProfile (): Observable<SelectionProfile> {
    return this.http.get<SelectionProfile>(this.url);
  }
}
