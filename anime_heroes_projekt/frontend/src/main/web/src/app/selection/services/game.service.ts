import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';

@Injectable()
export class GameService {

  private url = environment.apiBase + 'searchEnemy';

  constructor(private http: HttpClient) { }

  public getEnemyInformation(): Observable<GainedEnemyInformation> {
    let params = new HttpParams();
    params = params.append('var1', "alma");
    return this.http.get<GainedEnemyInformation>(this.url, {params: params});
  }

}
