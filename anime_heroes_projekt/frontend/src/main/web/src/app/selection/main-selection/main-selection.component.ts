import { Component, OnInit, OnDestroy, ViewChild, ComponentFactoryResolver } from '@angular/core';
import { CharacterSelectionService } from '../services/character-selection.service';
import { SelectionSelectedCharactersAndItemsComponent } from '../selection-selected-characters-and-items/selection-selected-characters-and-items.component';
import { SelectionCharactersComponent } from '../selection-characters/selection-characters.component';
import { Router } from '@angular/router';
import { SelectionState } from './selection-state';
import { SelectionInterface } from '../interfaces/selection-interface';
import { SelectionButtonImgSrc } from './selection-button-img-src';
import { NavigationService } from '../services/navigation.service';
import { SelectionNavigationBarComponent } from '../selection-navigation-bar/selection-navigation-bar.component';
import { GameStateService } from '../../game/services/game-state.service';
import { MainGameComponent } from '../../game/main-game/main-game.component';
import { ItemSelectionService } from '../services/item-selection.service';
import { SelectionStateService } from '../services/selection-state.service';

@Component({
  selector: 'app-main-selection',
  templateUrl: './main-selection.component.html',
  styleUrls: ['./main-selection.component.css'],
})
export class MainSelectionComponent implements OnInit, OnDestroy {

  @ViewChild(SelectionSelectedCharactersAndItemsComponent)
  private selectionSelectedCharactersAndItemsComponent: SelectionSelectedCharactersAndItemsComponent;

  @ViewChild(SelectionNavigationBarComponent)
  private selectionNavigationBarComponent: SelectionNavigationBarComponent;

  @ViewChild(MainGameComponent)
  private mainGameComponent: MainGameComponent;

  private selectionInterfaceImpl: SelectionInterface;

  private selectionState: string = SelectionState.NOTREADY;

  private selectionModes: string[] = ["selection/characters", "selection/items"];

  private actualModeNumber: number = 0;

  private urlImg: string = SelectionButtonImgSrc.ITEMS;

  public isLoadingPageActive: boolean = false;

  public isGameReady: boolean = false;

  private gameState: GameState;

  constructor(
    private characterSelectionService: CharacterSelectionService,
    private navigationService: NavigationService,
    private selectionStateService: SelectionStateService,
    private _router: Router,
    private gameStateService: GameStateService,
    private itemSelectionService: ItemSelectionService
  ) {}

  onActivate(componentRef){
    this.selectionInterfaceImpl = componentRef;
    componentRef.setStateOfCharacterSelection(this.selectionState);
  }

  routing(){
    if(this.actualModeNumber == 1){
      this.actualModeNumber = 0;
      this.urlImg = SelectionButtonImgSrc.ITEMS;
    }else{
      this.urlImg = SelectionButtonImgSrc.CHARS;
      this.actualModeNumber = 1;
    }
    this._router.navigate([this.selectionModes[this.actualModeNumber]]);
  }

  ngOnInit() {
    this.characterSelectionService.selectedCharacter$.subscribe(
      character => {
        if(!this.selectionSelectedCharactersAndItemsComponent.isAllCharacterSlotHasContent()){
          character.owned = false;
          this.selectionSelectedCharactersAndItemsComponent.setNextFreeCharacterSocket(character);
          if(this.selectionSelectedCharactersAndItemsComponent.isAllCharacterSlotHasContentFully()){
            this.selectionStateService.setStateOfCharacterSelection(SelectionState.READY);
            this.selectionStateService.sendEventFire();
            this.selectionNavigationBarComponent.setGameDisabledStateButtons(false);
          }
        }
      }
    );
    this.itemSelectionService.selectedItem$.subscribe(
      item => {
        if(this.selectionSelectedCharactersAndItemsComponent.isAllCharacterSlotHasContentFully()){
          this.selectionStateService.setStateOfCharacterSelection(SelectionState.READY);
          this.selectionStateService.sendEventFire();
          this.selectionNavigationBarComponent.setGameDisabledStateButtons(false);
        }
      }
    );
    this.characterSelectionService.unSelectedCharacter$.subscribe(
      characterName => {
        this.selectionStateService.setStateOfCharacterSelection(SelectionState.NOTREADY);
        this.selectionStateService.sendEventFire();
        this.selectionNavigationBarComponent.setGameDisabledStateButtons(true);
      }
    );
    this.navigationService.navigationRanked$.subscribe(
      value => {
        if(value){
          this.isLoadingPageActive = value;
          /*itt pedig van egy service hívás, ami enemyt keres.*/ 
          this.gameStateService.getGameState(this.selectionSelectedCharactersAndItemsComponent.getSelectedCharacters()).subscribe(
            state => {
              this.gameState = state;
              this.isGameReady = true;
              this.isLoadingPageActive = false;
            }
          );
        }
      }
    );
  }

  ngOnDestroy(): void {
    this.characterSelectionService.selectedCharacter.unsubscribe();
    this.characterSelectionService.unSelectedCharacter.unsubscribe();
    this.navigationService.navigationRanked.unsubscribe();
  }

  public isSelectionVisible(): boolean {
    return !this.isLoadingPageActive && !this.isGameReady;
  }
}
