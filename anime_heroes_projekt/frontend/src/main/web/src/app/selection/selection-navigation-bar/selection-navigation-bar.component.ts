import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { NavigationService } from '../services/navigation.service';

@Component({
  selector: 'app-selection-navigation-bar',
  templateUrl: './selection-navigation-bar.component.html',
  styleUrls: ['./selection-navigation-bar.component.css']
})
export class SelectionNavigationBarComponent{

  public rankedImg: string = "../../../assets/selection/Ranked.png";
  public quickImg: string = "../../../assets/selection/Quick.png";
  public privateImg: string = "../../../assets/selection/Private.png";
  public eventImg: string = "../../../assets/selection/Event.png";
  public logoutImg: string = "../../../assets/selection/Logout.png";

  public rankedDisabled: boolean = true;
  public quickDisabled: boolean = false;//TODO ez is legyen majd true
  public privateDisabled: boolean = true;
  public eventDisabled: boolean = true;
  public logoutDisabled: boolean = false;

  constructor(
    private navigationService: NavigationService,
    private _router: Router
  ) { }

  public onRankedHandler(): void{
    if(!this.rankedDisabled){
      this.navigationService.sendNavigationRanked();
      // this._router.navigate(['/gameload']);
    }
  }

  public onQuickHandler(): void{
    if(!this.quickDisabled){
      this.navigationService.sendNavigationRanked();
    }
  }

  public onPrivateHandler(): void{
    if(!this.privateDisabled){
      this.navigationService.sendNavigationRanked();
    }
  }

  public onEventHandler(): void{
    if(!this.eventDisabled){
      alert("event");
    }
  }

  public onLogoutHandler(): void{
    if(!this.logoutDisabled){
      alert("Logout");
    }
  }

  public setGameDisabledStateButtons(value: boolean){
    this.rankedDisabled = value;
    this.quickDisabled = value;
    this.privateDisabled = value;
  }
}
