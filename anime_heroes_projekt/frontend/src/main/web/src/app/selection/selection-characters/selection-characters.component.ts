import { Component, OnInit } from '@angular/core';
import { OwnedCharactersService } from '../services/owned-characters.service';
import { CharacterSelectionService } from '../services/character-selection.service';
import { SelectionInterface } from '../interfaces/selection-interface';
import { SelectionStateService } from '../services/selection-state.service';

@Component({
  selector: 'app-selection-characters',
  templateUrl: './selection-characters.component.html',
  styleUrls: ['./selection-characters.component.css']
})
export class SelectionCharactersComponent implements OnInit, SelectionInterface{

  public _stateOfCharacterSelection: string;

  public allCharacter: SelectionCharacter[];

  constructor(
    private ownedCharactersService: OwnedCharactersService,
    private characterSelectionService: CharacterSelectionService,
    private selectionStateService: SelectionStateService
  ) { }

  onCharacterDoubleClick(character: SelectionCharacter){
    if(character.owned){
      this.characterSelectionService.sendSelectedCharacter(character);
    }
  }

  ngOnInit() {
    this._stateOfCharacterSelection = this.selectionStateService.getStateOfCharacterSelection();
    this.selectionStateService.event$.subscribe(() =>{
      this._stateOfCharacterSelection = this.selectionStateService.getStateOfCharacterSelection();
    });
    this.ownedCharactersService.getData()
      .subscribe(all => {
        this.allCharacter = all;
      });
  }

  public setStateOfCharacterSelection(value: string): void {
    this._stateOfCharacterSelection = value;
  }
}
