import { Component } from '@angular/core';
import { CharacterSelectionService } from '../services/character-selection.service';
import { ItemSelectionService } from '../services/item-selection.service';

@Component({
  selector: 'app-selection-selected-characters-and-items',
  templateUrl: './selection-selected-characters-and-items.component.html',
  styleUrls: ['./selection-selected-characters-and-items.component.css']
})
export class SelectionSelectedCharactersAndItemsComponent{

  public selectedCharacters: SelectionCharacter[] = [
    null,
    null,
    null
  ]

  constructor(
    private characterSelectionService: CharacterSelectionService,
    private itemSelectionService: ItemSelectionService
  ) { }

  public setNextFreeCharacterSocket(character: SelectionCharacter): void{
    for(let i = 0; i < this.selectedCharacters.length; i++){
      if(this.selectedCharacters[i] == null){
        this.selectedCharacters[i] = character;
        break;
      }
    }
  }

  public removeCharacterFromSlot(character: SelectionCharacter): void{
    for(let i = 0; i < this.selectedCharacters.length; i++){
      if(this.selectedCharacters[i] == character){
        character.owned = true;
        this.characterSelectionService.sendUnSelectedCharacter(character.name);
        this.selectedCharacters[i] = null;
        character.items = [null, null];
        break;
      }
    }
  }

  /**Azt adja vissza, hogy minden slot minden itemmel ki van-e töltve */
  public isAllCharacterSlotHasContentFully(): boolean{
    for(let i = 0; i < this.selectedCharacters.length; i++){
      if(this.selectedCharacters[i] == null){
        return false;
      } else {
        for(let j=0;j<this.selectedCharacters[i].items.length;j++){
          if(this.selectedCharacters[i].items[j] == null){
            return false;
          }
        }
      }
    }
    return true;
  }

  public isAllCharacterSlotHasContent(): boolean{
    for(let i = 0; i < this.selectedCharacters.length; i++){
      if(this.selectedCharacters[i] == null){
        return false;
      }
    }
    return true;
  }

  transferDataSuccess($event: any, character: SelectionCharacter, row: number) {
    if($event.dragData != undefined && 
        (($event.dragData.isActive && this.hasActiveItemSelectedOnCharacter(character))
          || (!$event.dragData.isActive && this.hasNotActiveItemSelectedOnCharacter(character) 
          || (character.items[row] && character.items[row].isActive == $event.dragData.isActive)))){
      character.items[row] = $event.dragData;
      this.itemSelectionService.sendSelectedItem($event.dragData);
    }
  }

  public getSelectedCharacters(): SelectionCharacter[]{
    return this.selectedCharacters;
  }

  private hasActiveItemSelectedOnCharacter(character: SelectionCharacter): boolean{
    return character.items.filter(data =>{
      if(data != null){
        return data.isActive == true;
      } else {
        return false;
      }
    }).length == 0;
  }

  private hasNotActiveItemSelectedOnCharacter(character: SelectionCharacter): boolean{
    return character.items.filter(data =>{
      if(data != null){
        return data.isActive == false;
      } else {
        return false;
      }
    }).length == 0;
  }
}
