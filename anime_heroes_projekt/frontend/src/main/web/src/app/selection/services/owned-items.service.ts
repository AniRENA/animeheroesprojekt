import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { environment } from '../../../environments/environment';

@Injectable()
export class OwnedItemsService {

  private url = environment.apiBase + '/allItems';

  protected data: SelectionItem[] = null;
  protected subjectData: ReplaySubject<SelectionItem[]>;
  protected observableData: Observable<SelectionItem[]>;

  constructor(private http: HttpClient) {
    this.subjectData = new ReplaySubject(1);
    this.observableData = this.subjectData.asObservable(); 
  }

  public getData(): Observable<SelectionItem[]> {
    if (this.data == null) {
      this.getItems().subscribe(
        result => {
          this.subjectData.next(result);
          this.data = result;
        },
        err => this.subjectData.error(err)
      );
    }
    return this.observableData;
  }

  public resetCache(): void {
    this.data = null;
  }

  public refresh(): void {
    this.resetCache();
    this.getData();
  }

  private getItems(): Observable<SelectionItem[]> {
    return this.http.get<SelectionItem[]>(this.url);
  }
}
