import { Component, OnInit, Input } from '@angular/core';
import { OwnedItemsService } from '../services/owned-items.service';
import { SelectionInterface } from '../interfaces/selection-interface';
import { SelectionStateService } from '../services/selection-state.service';

@Component({
  selector: 'app-selection-items',
  templateUrl: './selection-items.component.html',
  styleUrls: ['./selection-items.component.css']
})
export class SelectionItemsComponent implements OnInit, SelectionInterface {

  public _stateOfCharacterSelection: string;

  public allItem: SelectionItem[] = [];

  constructor(
    private ownedItemsService: OwnedItemsService,
    private selectionStateService: SelectionStateService
    ) { }

  ngOnInit() {
    this._stateOfCharacterSelection = this.selectionStateService.getStateOfCharacterSelection();
    this.selectionStateService.event$.subscribe(() =>{
      this._stateOfCharacterSelection = this.selectionStateService.getStateOfCharacterSelection();
    });
    this.ownedItemsService.getData()
      .subscribe(all => {this.allItem = all});
  }

  public setStateOfCharacterSelection(value: string): void {
    this._stateOfCharacterSelection = value;
  }
}
