interface SelectionItem {
    name: string; 
	img: string;
	owned: boolean;
	isActive: boolean;
}
