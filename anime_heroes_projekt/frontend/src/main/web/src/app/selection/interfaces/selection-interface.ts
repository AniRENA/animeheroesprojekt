export interface SelectionInterface{
    setStateOfCharacterSelection(value: string): void;
}
