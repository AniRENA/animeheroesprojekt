export enum SelectionState {
    READY = "You are ready to battle!" ,
    NOTREADY = "You are not prepared!"
}
