import { Injectable } from '@angular/core';

/**
 * Ez az osztály egy általános tömb kezelő, bármilyen típusú tömbhöz tud adni, és törölni elemt.
 */
@Injectable()
export class ArrayService {

  constructor() { }

  public addLast(arraySource: any[], element: any): any[] {
    arraySource.splice(arraySource.length, 0, element);
    return arraySource;
  }

  public addFirst(arraySource: any[], element: any): any[] {
    arraySource.splice(0, 0, element);
    return arraySource;
  }

  public addInIndex(arraySource: any[], element: any, index: number): any[] {
    arraySource.splice(index, 0, element);
    return arraySource;
  }

  public remove(arraySource: any[], index: number): any[] {
    arraySource.splice(index, 1);
    return arraySource;
  }

  public mirror(arraySource: any[]): any[]{
    let result: any[] = [];
    for(let i = arraySource.length - 1; i >= 0 ; i--) {
      result = this.addLast(result, arraySource[i]);
    }
    return result;
  }
}
