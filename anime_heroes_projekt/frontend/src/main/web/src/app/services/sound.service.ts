import { Injectable } from '@angular/core';
import {Howl, Howler} from 'howler';

@Injectable()
export class SoundService {

  private winGameSong = new Howl({src: ['../../../assets/sounds/win.mp3']});

  private loseGameSong = new Howl({src: ['../../../assets/sounds/loose.mp3']});

  private attackGameSong = new Howl({src: ['../../../assets/sounds/attackEnemy.mp3']});

  private simpleClickGameSong = new Howl({src: ['../../../assets/sounds/simpleclick.mp3']});

  public canPlaySong: boolean = true;

  constructor() { }

  public playGameWinSong(): void{
    if(this.canPlaySong){
      this.winGameSong.play();
    }
  }

  public playLoseGameSong(): void{
    if(this.canPlaySong){
      this.loseGameSong.play();
    }
  }

  public playAttackGameSong(): void{
    if(this.canPlaySong){
      this.attackGameSong.play();
    }
  }

  public playSimpleClickGameSong(): void{
    if(this.canPlaySong){
      this.simpleClickGameSong.play();
    }
  }

}
