import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class LoginService {

  private url = '/login';

  constructor(private http: HttpClient) { }

  login(): Observable<void>{
    console.log("send");
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Access-Control-Allow-Origin':  '*'
      })
    };
    return this.http.post<void>(this.url, {username:"fremen", password:"pw"}, httpOptions);
  }

}
