import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable()
export class RegistrationService {

  private url = environment.apiBase + '/registration';

  constructor(private http: HttpClient) { }

  public onRegisterHandler(username: string, password: string, email: string): Observable<any> {
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Access-Control-Allow-Origin':  '*',
        'Access-Control-Allow-Methods': 'POST'
      })
    };
    return this.http.post<any>(this.url, {username: username, password: password, email: email}, httpOptions);
  }

}
