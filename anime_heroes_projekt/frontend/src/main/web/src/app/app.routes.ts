import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { MianpageComponent } from './mianpage/mianpage.component';
import { MainSelectionComponent } from './selection/main-selection/main-selection.component';
import { SelectionCharactersComponent } from './selection/selection-characters/selection-characters.component';
import { SelectionSelectedCharactersAndItemsComponent } from './selection/selection-selected-characters-and-items/selection-selected-characters-and-items.component';
import { SelectionItemsComponent } from './selection/selection-items/selection-items.component';
import { MainGameComponent } from './game/main-game/main-game.component';
import { RegistrationComponent } from './mianpage/registration/registration.component';
import { WebsocketComponent } from './game/websocket/websocket/websocket.component';

const appRoutes = [
    {path: '', component: MianpageComponent},
    {
      path: 'selection',
      component: MainSelectionComponent,
      children: [
        {
          path: 'characters',
          component: SelectionCharactersComponent
        },
        {
          path: 'items',
          component: SelectionItemsComponent
        }
      ]
    },
    {
      path: 'game',
      component: MainGameComponent
    },
    {
      path : 'registration',
      component: RegistrationComponent
    },
    {
      path : 'websocket',
      component: WebsocketComponent
    }
]
 
@NgModule({
  imports: [ RouterModule.forRoot(appRoutes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {}
