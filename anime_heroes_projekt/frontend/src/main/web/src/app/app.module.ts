import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule }    from '@angular/common/http';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routes';
import { MianpageComponent } from './mianpage/mianpage.component';
import { MainSelectionComponent } from './selection/main-selection/main-selection.component';
import { SelectionCharactersComponent } from './selection/selection-characters/selection-characters.component';
import { SelectionItemsComponent } from './selection/selection-items/selection-items.component';
import { SelectionSelectedCharactersAndItemsComponent } from './selection/selection-selected-characters-and-items/selection-selected-characters-and-items.component';
import { SelectionNavigationBarComponent } from './selection/selection-navigation-bar/selection-navigation-bar.component';
import { SelectionUserProfileComponent } from './selection/selection-user-profile/selection-user-profile.component';
import { SelectionUserServiceService } from './selection/services/selection-user-service.service';
import { OwnedCharactersService } from './selection/services/owned-characters.service';
import { CharacterSelectionService } from './selection/services/character-selection.service';
import { DndModule } from 'ng2-dnd';
import { OwnedItemsService } from './selection/services/owned-items.service';
import { MainGameComponent } from './game/main-game/main-game.component';
import { NavigationService } from './selection/services/navigation.service';
import { GameService } from './selection/services/game.service';
import { GameUserProfilesComponent } from './game/game-user-profiles/game-user-profiles.component';
import { GameComponent } from './game/game/game.component';
import { TurnHandlerComponent } from './game/turn-handler/turn-handler.component';
import { CharacterDescriptorComponent } from './game/character-descriptor/character-descriptor.component';
import { GameStateService } from './game/services/game-state.service';
import { DescriptionService } from './game/services/description.service';
import { CostService } from './game/services/cost.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ItemSelectionService } from './selection/services/item-selection.service';
import { SoundService } from './services/sound.service';
import { SurrenderComponent } from './game/surrender/surrender.component';
import { GameMenuClickService } from './game/services/game-menu-click.service';
import { GameResultComponent } from './game/game-result/game-result.component';
import { ExchangeComponent } from './game/exchange/exchange.component';
import { TurnChangerComponent } from './game/turn-changer/turn-changer.component';
import { LoginService } from './services/login.service';
import { RegistrationComponent } from './mianpage/registration/registration.component';
import { FormsModule } from '@angular/forms';
import { RegistrationService } from './services/registration.service';
import { ArrayService } from './services/array.service';
import { UsedSkillsService } from './game/services/used-skills.service';
import { SelectionStateService } from './selection/services/selection-state.service';
import { BuffOrderService } from './game/services/buff-order.service';
import { TurnService } from './game/services/turn.service';
import { HealthBarService } from './game/services/health-bar.service';
import { TurnChangePrePhaseService } from './game/services/turn-change-pre-phase.service';
import { WebsocketComponent } from './game/websocket/websocket/websocket.component';
import { RankService } from './game/services/rank.service';
import { WebsocketService } from './game/services/websocket.service';
import { EndGameService } from './game/services/end-game.service';


@NgModule({
  declarations: [
    AppComponent,
    MianpageComponent,
    MainSelectionComponent,
    SelectionCharactersComponent,
    SelectionItemsComponent,
    SelectionSelectedCharactersAndItemsComponent,
    SelectionNavigationBarComponent,
    SelectionUserProfileComponent,
    MainGameComponent,
    GameUserProfilesComponent,
    GameComponent,
    TurnHandlerComponent,
    CharacterDescriptorComponent,
    SurrenderComponent,
    GameResultComponent,
    ExchangeComponent,
    TurnChangerComponent,
    RegistrationComponent,
    WebsocketComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    DndModule.forRoot(),
    NgbModule.forRoot(),
    DndModule.forRoot()
  ],
  providers: [
    SelectionUserServiceService, 
    OwnedCharactersService, 
    CharacterSelectionService,
    OwnedItemsService,
    NavigationService,
    GameService,
    GameStateService,
    DescriptionService,
    CostService,
    ItemSelectionService,
    SoundService,
    GameMenuClickService,
    LoginService,
    RegistrationService,
    ArrayService,
    UsedSkillsService,
    SelectionStateService,
    BuffOrderService,
    TurnService,
    HealthBarService,
    TurnChangePrePhaseService,
    RankService,
    WebsocketService,
    EndGameService
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
