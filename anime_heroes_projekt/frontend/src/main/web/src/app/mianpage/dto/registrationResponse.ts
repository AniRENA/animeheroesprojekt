enum RegistrationResponse{
    EXISTING_USERNAME,
    TOO_LONG_USERNAME,
    PASSWORDS_ARE_DIFFERENT,
    WEAK_PASSWORD,
    WRONG_EMAIL_FORMAT,
    CHECK_ROBOT_VALIDATOR,
    CANNOT_BE_EMPTY_FIELD,
    OK
}
/*A szervertől ilyen válaszokat kaphatunk. */