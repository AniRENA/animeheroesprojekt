import { Component, OnInit } from '@angular/core';
import { RegistrationService } from '../../services/registration.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css', '../../../bootstrap.css']
})
export class RegistrationComponent{

  public username: string;

  public password: string;

  public email: string;

  public responseCode: string;

  constructor(private registrationservice: RegistrationService) {}

  public onRegisterHandler(): void {
    this.registrationservice.onRegisterHandler(this.username, this.password, this.email)
    .subscribe(code => {this.responseCode = code;console.log(code)});
  }

}
