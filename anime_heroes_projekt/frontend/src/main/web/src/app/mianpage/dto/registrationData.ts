interface RegistrationData {
    username: string;
    password: string;
    email: string;
}