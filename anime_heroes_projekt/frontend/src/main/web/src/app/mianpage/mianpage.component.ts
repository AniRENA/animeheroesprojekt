import { Component, OnInit } from '@angular/core';
import { Character } from '../Character';
import { Router } from '@angular/router';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-mianpage',
  templateUrl: './mianpage.component.html',
  styleUrls: ['./mianpage.component.css', '../../bootstrap.css']
})
export class MianpageComponent {

  public userName: string;

  public password: string;

exampleVariales = ['alma', 'körte', 'szilva'];
meret: number | null = 14;
character: Character = null;
todos: string[] = ["alma", "körte", "szilva"];
  constructor( 
    private _router: Router,
    private loginService: LoginService
  ){
  }

  public callService(): void{
  }

  public goToTheGame(a: string, b:string): void{
    console.log(a);
    console.log(b);
    this.loginService.login().subscribe();
    //this._router.navigate(['/selection/characters']);
  }

  public onMove(todo: string, position: number):void {
  }
}

/**TODO Ezt az osztályt megvizsgálni, hogy kell-e, mert úgy látszik, hogy gyakorlásra volt használva. */
